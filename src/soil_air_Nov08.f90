
SUBROUTINE BOUNDARY()
! Determine boundary layer conductances at each canopy layer
USE VEG
USE CLIM
USE METEO
IMPLICIT NONE

INTEGER i, t
REAL roughl
REAL alpha, thick, xx, tempx
REAL Dwv, store, mult
REAL u(numl,steps)
      
alpha = 4.
roughl = 0.075*towerht		! Tower height is canht+4

DO t=1,steps
	DO i=1,numl
		gbw(i,t)=0.
	ENDDO
    xx = 0.
    store = 1.0
    DO i=1,numl
		xx = xx+1.
		tempx = temptop(t)-0.1111*(xx-1)*(temptop(t)-tempbot(t))
		Dwv = .0000242*(((tempx+273.15)/293.15)**1.75)*101300./atmos_pres	! Jones p 51
        ! Windspeed in layer - no decay in windspeed in this open stand
        !u(i,t) = windsp(t)
		mult = EXP(alpha*(layerht(i)/towerht-1.))
		u(i,t) = windsp(t)*mult
		IF(u(i,t).le.0.) THEN
			WRITE(*,*)daynum,t,windsp(t) 
			WRITE(*,'("Windspeed ERROR")')
		ENDIF
		thick = 0.004*(dimen/u(i,t))**0.5	! Boundary layer thickness
        ! Conductance to water vapour (m s-1 - not mol m-2 s-1 as in Amthor p.333)
        ! i.e. remove P/RT
		gbw(i,t) = Dwv/thick
    ENDDO
ENDDO
RETURN

END

!-------------------------------------------------------------------------------------!

SUBROUTINE SOILFC()		! From Saxton et al (1986)
! Waterfrac is m3 m-3, soilwp is MPa
! Conductivity corrected 12 Feb 1999
    USE HYDROL
    USE SOIL_STRUCTURE
    USE HOURSCALE	        ! Contains soil evap estimate Qe
    USE CLIM
    USE METEO

    IMPLICIT NONE

    INTEGER :: i
    REAL :: KS, K, THETA_S, LAMBDA, B, S, C, OM
    REAL :: THETA1500, THETA33, THETA33S
    REAL :: THETA1500_T, THETA33_T, THETA33S_T
    REAL :: watercontent


    waterloss = 0.0     ! RESET
    watergain = 0.0     ! RESET
    pptgain = 0.0       ! RESET

    DO i=1,core	

        S = sandpc(i)/100.0     ! Convert from % to fraction
        C = claypc(i)/100.0     ! Convert from % to fraction
        OM = organicfrac(i)
        
        ! Equation 1 from new Saxton Table (Saxton et al. 2006)
        THETA1500_T = -0.024*S + 0.487*C + 0.006*OM + &
                      0.005*( S*OM ) - 0.013*( C*OM ) + &
                      0.068*( S*C ) + 0.031
                      
        THETA1500 = THETA1500_T + (0.14*THETA1500_T - 0.02)
        
        ! Equation 2 from new Saxton Table (Saxton et al. 2006)
        THETA33_T = -0.251*S + 0.195*C + 0.011*OM + &
                    0.006*( S*OM ) - 0.027*( C*OM ) + &
                    0.452*( S*C ) + 0.299
                
        THETA33 = THETA33_T + (1.283*(THETA33_T**2.0) - 0.374*THETA33_T - 0.015)
        
        ! Equation 3 from new Saxton Table (Saxton et al. 2006)
        THETA33S_T = 0.278*S + 0.034*C + 0.022*OM - &
                     0.018*( S*OM ) - 0.027*( C*OM ) - &
                     0.584*( S*C ) + 0.078
        
        THETA33S = THETA33S_T + (0.636*THETA33S_T - 0.107)
        
        ! Calculate porosity (Field Capacity/Saturated Water Content)
        IF(extra.EQ.0.0) THEN
            porosity(i) = THETA33 + THETA33S - 0.097*S + 0.043  ! Equation 5 from new Saxton Table (Saxton et al. 2006)
        END IF
        
        watercontent = (waterfrac(i)*thickness(i)-waterloss(i)+watergain(i))/thickness(i)
        !watercontent = waterfrac(i)
        
        ! Calculate hydraulic soil conductivity
        IF(waterfrac(i).LT.0.05) THEN
            conduc(i) = 1.0E-30                                               ! Avoid underflow problem
        ELSE   
            B = ( LOG(1500.0)-LOG(33.0) )/( LOG(THETA33)-LOG(THETA1500) )     ! Equation 15 from new Saxton Table (Saxton et al. 2006)
            LAMBDA = 1.0/B                                                    ! Equation 18 from new Saxton Table (Saxton et al. 2006)
            KS = 1930.0*(porosity(i) - THETA33)**(3.0-LAMBDA)                 ! Equation 16 from new Saxton Table (Saxton et al. 2006)
            K = KS*( watercontent/porosity(i) )**( 3.0+(2.0/LAMBDA) )         ! Equation 17 from new Saxton Table (Saxton et al. 2006)
            conduc(i) = K/(1000.0*3600.0)                                     ! Convert from mm/hr to m/s
        END IF

    END DO

    RETURN

END SUBROUTINE SOILFC

! --------------------------------------------------------------------
SUBROUTINE SWPSOILR()
    ! Waterfrac is m3 m-3, soilwp is MPa
    ! Conductivity corrected 12 Feb 1999
    USE HYDROL
    USE SOIL_STRUCTURE
    USE HOURSCALE	! Contains soil evap estimate Qe
    USE CLIM
    USE METEO

    IMPLICIT NONE

    INTEGER :: i
    REAL :: soil_WP, soil_sat, maxloss, unsat, rs, Lsoil, rs2, pi, maxdrain
    REAL :: watercontent, rrcheck, numsecs
    REAL :: KS, K, THETA_S, LAMBDA, A, B, S, C, OM
    REAL :: THETA1500, THETA33
    REAL :: THETA1500_T, THETA33_T


    numsecs = 3600.*24./REAL(steps)	!# 0f seconds per timestep
    pi = 3.14159
    lambda = 1000.*(2501.0-2.364*(hourtemp-273.15))	! latent heat of vapourisation, J kg-1
    soilr = 0.0

    DO i=1,layer		! Find SWP without updating waterfrac yet (DO that in waterthermal)


        S = sandpc(i)/100.0     ! Convert from % to fraction
        C = claypc(i)/100.0     ! Convert from % to fraction
        OM = organicfrac(i)    
    	
        ! Equation 1 from new Saxton Table (Saxton et al. 2006)
        THETA1500_T = -0.024*S + 0.487*C + 0.006*OM + &
                      0.005*( S*OM ) - 0.013*( C*OM ) + &
                      0.068*( S*C ) + 0.031
                      
        THETA1500 = THETA1500_T + (0.14*THETA1500_T - 0.02)
        
        ! Equation 2 from new Saxton Table (Saxton et al. 2006)
        THETA33_T = -0.251*S + 0.195*C + 0.011*OM + &
                    0.006*( S*OM ) - 0.027*( C*OM ) + &
                    0.452*( S*C ) + 0.299
                
        THETA33 = THETA33_T + (1.283*(THETA33_T**2.0) - 0.374*THETA33_T - 0.015)
        
        ! Calculate the soil water content
	    watercontent = (waterfrac(i)*thickness(i)-waterloss(i)+watergain(i))/thickness(i)
	    !watercontent = waterfrac(i)
    	
	    B = ( LOG(1500.0)-LOG(33.0) )/( LOG(THETA33)-LOG(THETA1500) )   ! Equation 15 from new Saxton Table (Saxton et al. 2006)
	    A = EXP( LOG(33.0) + B*LOG(THETA33)  )                          ! Equation 14 from new Saxton Table (Saxton et al. 2006)
    	
	    IF(watercontent.gt.0.0) THEN
		    soil_WP = A*watercontent**(-B)      ! Equation 11 from new Saxton Table (Saxton et al. 2006)
		    soil_WP = -0.001*soil_WP            ! Soil water potential (MPa)
	    ELSE
		    soil_WP = -9999.
	    ENDIF

	    IF(saxton.eq.0) THEN	! Turn off Saxton water potential
		    soil_WP = -1.21+4.47*watercontent	! 7 June 1999, empirical relationship from Bev for Oregon soils
	    ENDIF

	    SWP(i) = soil_WP
	    ! Calculate soil-root hydraulic resistance
	    Lsoil=conduc(i)/head	! Converts from ms-1 to m2 s-1 MPa-1
	    IF(Lsoil.lt.1e-35) THEN	! Prevent floating point error
		    soilr(i) = 1e35
	    ELSE 
		    ! Exploited soil volume = 1.0 * soil depth; each canopy layer has lrv/numl root length and is soildp/numl deep - so the numl cancel
		    !rs = SQRT(thickness(i)/(rootlength*pi))
		    rs = SQRT(1./(rootlength(i)*thickness(i)*pi))
		    !lrv = total length of roots, must be divided by number of layers in this case
		    rs2 = LOG(rs/rootrad)/(2.0*pi*rootlength(i)*thickness(i)*Lsoil)
		    soilR1(i) = rs2*1E-6*18*0.001	! convert from MPa s m2 m-3 to MPa s m2 mmol-1
		    ! Second component of below ground resistance related to root hydraulics
		    !rrcheck = rootresist/(rootmass(i)*thickness(i)/abovebelow)
		    soilR2(i) = rootresist/(rootmass(i)*thickness(i)/abovebelow)
		    soilR(i) = soilR1(i)+soilR2(i)
	    END IF
    END DO

    RETURN

END SUBROUTINE SWPSOILR

!-------------------------------------------------------------------------------------!

REAL FUNCTION SOILCOND(wf)		! For soil drainage INTEGRATOR
! Waterfrac is m3 m-3, soilwp is MPa
! Conductivity corrected 12 Feb 1999
    USE HYDROL
    USE SOIL_STRUCTURE
    USE HOURSCALE	! Contains soil evap estimate Qe
    USE CLIM
    USE METEO
    USE SOILINFO

    IMPLICIT NONE

    INTEGER :: i
    REAL :: KS, K, THETA_S, LAMBDA, B, S, C, OM
    REAL :: THETA1500, THETA33, THETA33S
    REAL :: THETA1500_T, THETA33_T, THETA33S_T
    REAL :: watercontent, wf

    DO i=1,core	

        S = sandpc(i)/100.0     ! Convert from % to fraction
        C = claypc(i)/100.0     ! Convert from % to fraction
        OM = organicfrac(i)
        
        ! Equation 1 from new Saxton Table (Saxton et al. 2006)
        THETA1500_T = -0.024*S + 0.487*C + 0.006*OM + &
                      0.005*( S*OM ) - 0.013*( C*OM ) + &
                      0.068*( S*C ) + 0.031
                      
        THETA1500 = THETA1500_T + (0.14*THETA1500_T - 0.02)
        
        ! Equation 2 from new Saxton Table (Saxton et al. 2006)
        THETA33_T = -0.251*S + 0.195*C + 0.011*OM + &
                    0.006*( S*OM ) - 0.027*( C*OM ) + &
                    0.452*( S*C ) + 0.299
                
        THETA33 = THETA33_T + (1.283*(THETA33_T**2.0) - 0.374*THETA33_T - 0.015)
        
        ! Equation 3 from new Saxton Table (Saxton et al. 2006)
        THETA33S_T = 0.278*S + 0.034*C + 0.022*OM - &
                     0.018*( S*OM ) - 0.027*( C*OM ) - &
                     0.584*( S*C ) + 0.078
        
        THETA33S = THETA33S_T + (0.636*THETA33S_T - 0.107)
        
        ! Calculate porosity (Field Capacity/Saturated Water Content)
        IF(extra.EQ.0.0) THEN
            THETA_S = THETA33 + THETA33S - 0.097*S + 0.043  ! Equation 5 from new Saxton Table (Saxton et al. 2006)
        END IF
        
        watercontent = (wf*thickness(i)-waterloss(i)+watergain(i))/thickness(i)
        !watercontent = wf
        
        ! Calculate hydraulic soil conductivity
        IF(wf.LT.0.05) THEN
            soilcond = 1.0E-30                                               ! Avoid underflow problem
        ELSE   
            B = ( LOG(1500.0)-LOG(33.0) )/( LOG(THETA33)-LOG(THETA1500) )    ! Equation 15 from new Saxton Table (Saxton et al. 2006)
            LAMBDA = 1.0/B                                                   ! Equation 18 from new Saxton Table (Saxton et al. 2006)
            KS = 1930.0*(THETA_S - THETA33)**(3.0-LAMBDA)                    ! Equation 16 from new Saxton Table (Saxton et al. 2006)
            K = KS*( watercontent/porosity(i) )**( 3.0+(2.0/LAMBDA) )        ! Equation 17 from new Saxton Table (Saxton et al. 2006)
            soilcond = K/(1000.0*3600.0)                                     ! Convert from mm/hr to m/s
        END IF
    END DO
    
    RETURN

END FUNCTION SOILCOND