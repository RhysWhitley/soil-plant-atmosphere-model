! ==================================================================
! ================== SOIL SUBROUTINES & FUNCTIONS ==================
! ==================================================================
!
! 5 Dec 02.  Incorporates snow model
! Crank Nicholson model to solve temperature flux in soil profile
! new latent energy flux model (based on SWP of surface layer)
!

SUBROUTINE SOILDAY(time,totestevap)

USE ERROR
USE CLIM
USE HOURSCALE
USE SOIL_STRUCTURE
USE IRRADIANCE_SUNSHADE
USE VEG
USE SNOWINFO
IMPLICIT NONE

INTEGER i, time, xx, j, k, p
REAL xacc, t1, t2, ts, vpd, rs, rootwater(rootl), watercont(layer), top4, lambda
REAL waterchange, fluxsum, checkerror, numsecs, cw, pw, delw, checkdiff, totestevap
!REAL f_I, snowheat, snowheight, snowweight, W_L, soilcond
REAL ZBRENT, energy, exco, thermcond       ! <=== Functions
EXTERNAL energy

numsecs = 3600.*24./steps   ! # 0f seconds per timestep

! ==== SPECIFY TIME DRIVERS INTO NEW VARIABLES ====
pw = SUM(watericemm)                    ! Total ice water in the soil (mm) ????
xacc = 0.0001                           
Qh = 0.; Qe = 0.; Qn = 0.; Qc = 0.      ! Surface energy balance fluxes
hour = time; i = time
hourtemp = tempbot(i)+freeze            ! Convert from oC to K
hourvpd = 0.1*wdbot(i)*hourtemp/217.
hourwind = windsp(i); errtime = daytime(i); hourrad = soilnet(i); hourpress = pressure(i)
hourtime = daytime(i); hourppt = ppt(i); hourrnet = modrnet(i)
surface_watermm = 0.                                    ! Assume that water infiltrates within a timestep
lambda = 1000.*(2501.0-2.364*(hourtemp-273.15))         ! Latent heat of vapourisation, J kg-1

CALL SOILFC()                   ! Determine field capacity, [SOILAIR.f90]
waterloss = 0; watergain = 0
CALL SWPSOILR()                 ! Determine soil water potential and soil-root resistance, [SOILAIR.f90]

! === IT IS SNOWING ===
IF (((ppt(i).gt.0.).and.(temptop(i).lt.0.)).or.(snowweight.gt.0.)) THEN         ! IF it is snowing or if there is snow on the ground, call the snow routines                                                                       
   
    ! We make a simplification by assuming no interception of snow by the canopy
    TS = -totsoilflux/(3600*thermcond(1))*0.5*(thickness(1))+soiltemp(2)	        ! Calculate soil surface temp from flux of energy between lowest snow layer and soil surface
    
	CALL SNOWRUN()
    IF (snowheight.gt.0.006) THEN	! If there is snow then no further energy balance calcs are required
        Qh = 0.
        Qe = 0.
        Qn = 0.
    ELSE	! But if snow is too thin, then do a proper energy balance
        t1 = hourtemp-50.; t2 = hourtemp+50.  ! Set temp bounds based on air temp
        TS = ZBRENT(ENERGY,t1,t2,xacc)        ! Calculate surface temp based on energy balance
    ENDIF
    surface_watermm = totwaterflux*1000.	    ! How much water reaches the soil surface from melting snow?

! ===*** IT IS NOT SNOWING, NOR IS THERE ANY SNOW ***===
ELSE		
    totwaterflux = 0.
    CALL CANOPY_BALANCE(i)                   ! Interception of rain and canopy evap
    wetev(i) = evap_store                    ! mm t-1
   
    t1 = hourtemp-50.; t2 = hourtemp+50.     ! Set temp bounds based on air temp
    TS = ZBRENT(ENERGY,t1,t2,xacc)           ! Calculate surface temp based on energy balance
ENDIF

IF(SUM(lai).gt.0.) THEN                     ! There are Leaves on trees --> Transpiration is ON
    CALL WATERUPTAKELAYER(totestevap)		! O/P = totestevap
ENDIF

hourts = ts
CALL WATERFLUXES(i)                         ! Gains and losses
CALL WATERTHERMAL(i)                        ! Apply thermal corrections and implement water fluxes

soiltemp_nplus1 = 0
soiltemp(1) = ts
soiltemp_nplus1(1) = ts
soiltemp_nplus1(core) = soiltemp(core)
CALL CRANK_NICHOLSON()                      ! Soil temperature profile
CALL THAWDEPTH(ts)                          ! Thaw depths and icefractions

ess(i) = dayevap                            ! Wm-2
lambda = 1000.*(2501.0-2.364*(ts-273.15))   ! Latent heat of vapourisation, J kg-1
soiletmm(i) = ess(i)/lambda*numsecs         ! Convert to mm t-1

top4 = 0. ! Water stored in top 4 layers
DO p=1,4
    top4 = top4+waterfrac(p)*thickness(p)
ENDDO
                                         

waterchange = 1e3*(SUM(pptgain)+SUM(watergain)-watergain(core))-1e3*(SUM(waterloss)-&
                waterloss(core))
fluxsum = soiletmm(i)+1e3*(overflow+totet+underflow)-surface_watermm
checkerror = waterchange+fluxsum
cw = SUM(watericemm)
delw = pw-cw

checkdiff = delw-fluxsum 

IF (outt.eq.1) THEN
    WRITE(40,'(25(F9.2," ,  "))')daytime(i),Qh,Qe,Qn,Qc,hourtemp-freeze,ts-freeze,soiltemp(2)-freeze,drythick*1e3  
	WRITE(101,'(28(F10.5,", "))')daytime(i),1e3*overflow,1e3*totet,1e3*underflow,&
		surface_watermm,SUM(watericemm),waterchange,fluxsum,checkerror,1e3*watergain(1),&
		1e3*waterloss(1),delw,checkdiff,SUM(pptgain),SUM(watergain),SUM(waterloss),&
		watergain(core),waterloss(core),snowweight,snowheight
ENDIF  

IF((time.eq.24).or.(outt.eq.1)) THEN
    WRITE(60,'(22(F9.4," , "))')daytime(i),(waterfrac(p),p=1,15),weighted_swp
    WRITE(68,'(22(F9.4," , "))')daytime(i),(iceprop(p),p=1,15)       
	WRITE(83,'(40(F9.4," , "))')daytime(i),(soiltemp(p)-273.15,p=1,15),tempbot(i)
ENDIF

IF(time.eq.24) THEN
    WRITE(62,'(22(F9.4," , "))')daytime(i),(fraction_uptake(p),p=1,15)
ENDIF


RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
SUBROUTINE WATERUPTAKELAYER(totestevap)         ! From which layer is water withdrawn?  15 Oct 1999

USE CANOPY
USE HOURSCALE
USE SOIL_STRUCTURE
USE VEG
USE METEO
USE METAB
IMPLICIT NONE

INTEGER i, j, p
REAL estevap(core), totestevap, frac
INTEGER, ALLOCATABLE :: AR2(:)  ! Put values in array
j = SIZE(SHAPE(estevap))        ! Get number of dimensions in array
ALLOCATE ( AR2(j))              ! Allocate AR1 to number of dimensions in array

totestevap = 0.; weighted_SWP = 0.; estevap = 0.; fraction_uptake = 0.

! Estimated max transpiration from gradient-gravity / soil resistance FOR EACH SOIL LAYER
DO i=1, rootl                               
    estevap(i) = (swp(i) - minlwp)/(soilR(i))   ! MAXIMUM POTENTIAL WATER UPTAKE
    estevap(i) = MAX(0.,estevap(i))             ! NO NEGATIVE 
    IF(iceprop(i).gt.0.) estevap(i) = 0.        ! No uptake from frozen soils <==== SNOW CHECK
ENDDO
totestevap = SUM(estevap)                       ! SUM OF ALL ESTIMATED EVAPORATION FROM ALL SOIL LAYERS

! Weighted soil water potential
DO i=1, rootl
    weighted_SWP = weighted_SWP+swp(i)*estevap(i)
    IF(totestevap.gt.0.) THEN
        fraction_uptake(i) = estevap(i)/totestevap   ! Fraction of total et taken from layer i
    ELSE
        estevap = 0.
        fraction_uptake(i) = 1./rootl                ! IF totestevap <= 0 THEN FRACTION UPTAKE IS DIVIDED EVENLY BETWEEN SOIL LAYERS
    ENDIF
ENDDO
weighted_SWP = weighted_SWP/totestevap

! ERROR CHECKING FOR FRACTIONAL WATER UPTAKE IN SOIL LAYERS
IF(NINT(SUM(fraction_uptake)).ne.1.) THEN
    WRITE(*,*)'=== PROBLEM IN SR WATERUPTAKELAYER ==='
    WRITE(*,*)'nint(sum(fraction_uptake)).ne.1. in wateruptake layer'
ENDIF
IF((fraction_uptake(1).gt.1).or.(fraction_uptake(1).lt.0.)) THEN
    WRITE(*,*)'=== PROBLEM IN SR WATERUPTAKELAYER ==='
    WRITE(*,*)'frac up prob'
ENDIF

! TOTAL ROOT AND HYDRAULIC RESISTANCE IS CALCULATED FOR EACH CANOPY LAYER
canopy_soil_resistance = 0.	! RESET
DO p=1,numl                 ! CANOPY LAYER p
    frac = lai(p)/SUM(lai)
    DO i=1, rootl           ! ROOT LAYER i
        IF (frac.gt.0.) THEN 
            canopy_soil_resistance(p) = canopy_soil_resistance(p) + 1./(soilR(i)/frac)  ! Soil resistance for each canopy layer is related to leaf area
        ELSE
            canopy_soil_resistance(p) = 0.001
        ENDIF 
    ENDDO
    canopy_soil_resistance(p) = 1./canopy_soil_resistance(p)
ENDDO

!IF(weighted_soilR.lt.0.) THEN
!   WRITE(*.*)'ERROR: Weighted soil resistance is less than zero!'
!   PAUSE
!ENDIF

RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
! Finite difference PDE solver for soil temperature profile
SUBROUTINE CRANK_NICHOLSON()

USE CANOPY
USE INTEGRATOR
USE HOURSCALE
USE SOIL_STRUCTURE
IMPLICIT NONE

INTEGER i, iter_no
REAL D, old_value, error, beta            ! Value of previous iteration, error for convergence monitoring */
REAL tdiffuse, max_error, numsecs
REAL heatcap    ! <=== Function
REAL thermcond  ! <=== Function
iter_no = 0
MAX_ERROR = 0.0000005
beta = 0.5
numsecs = 3600.*24./steps ! # 0f seconds per timestep
 
error = 1.
DO WHILE (error.gt.max_error)                                      
    error = 0.  ! Reset error */
    i = 2                      
    DO WHILE (i.lt.core)    !* loop for all x-dimension nodes, except first and last */
        thermal = thermcond(i)
        tdiffuse = numsecs*thermal/heatcap(i)     ! Walbroeck thermal conductivity, W m-1 K-1 is converted to J m-1 K-1 timestep-1
        D = (tdiffuse)/(thickness(i)*thickness(i));     
        old_value = soiltemp_nplus1(i)            ! store value of previous iteration */
        !* Calculate the temperature at the new time step using an implicit method */
        soiltemp_nplus1(i) = (D/(1+2*BETA*D))&
            * (BETA*(soiltemp_nplus1(i+1) + soiltemp_nplus1(i-1))&
            +   (1-BETA)*(soiltemp(i+1) - 2*soiltemp(i)+ soiltemp(i-1)))&
            +  soiltemp(i)/(1+2*BETA*D);
        error = error+ABS(old_value-soiltemp_nplus1(i))  !* calculate the error */
        i=i+1
    ENDDO
ENDDO
 
 ! Loop for all x-dimension nodes, except first and last */
DO i=2, core-1               
     soiltemp(i) = soiltemp_nplus1(i)           ! Set the values at time n equal to the values at */
ENDDO                                           ! Time   n+1 for the next time step */
 
RETURN
END
! -------------------------------------------------------------------- 
 
 

! --------------------------------------------------------------------
SUBROUTINE SOIL_BALANCE(soillayer)      !integrator for soil gravitational drainage

USE SOIL_STRUCTURE
USE INTEGRATOR
USE HOURSCALE
USE SOILINFO
IMPLICIT NONE

INTEGER soillayer
INTEGER NVAR,i
PARAMETER (NVAR=1)
INTEGER kmax, kount, nbad, nok
REAL dxsav, eps, h1, hmin, x(KMAXX), y(NMAX,KMAXX), x1, x2
REAL dstore_dt, ystart(nvar), change, newwf!, rkqs
EXTERNAL SOILSTOR, RKQS
COMMON /path/ kmax, kount, dxsav, x, y

eps = 1.0e-4; h1 = .001; hmin = 0.0; kmax = 100; x1 = 1.; x2 = 2.
dxsav = (x2-x1)/15.0	! I assume this '20' is the number of soil layers (must be changed if changing SLAYERS)

soilpor = porosity(soillayer)
liquid = waterfrac(soillayer)*(1.-iceprop(soillayer))     ! Liquid fraction
drainlayer = draincheck(soillayer)
unsat = MAX(0.,(porosity(soillayer+1)-waterfrac(soillayer+1))*thickness(soillayer+1)/thickness(soillayer))  ! Unsaturated volume of layer below, m3 m-2
slayer = soillayer

! Initial conditions
IF((liquid.gt.0.).and.(waterfrac(soillayer).gt.drainlayer*soilpor)) THEN    ! If there is liquid water
        ystart(1) = waterfrac(soillayer)          ! Total layer   
        CALL ODEINT(ystart,nvar,x1,x2,eps,h1,hmin,nok,nbad,SOILSTOR,RKQS)
        newwf = ystart(1)
        change = (waterfrac(soillayer)-newwf)*thickness(soillayer)        ! Convert from waterfraction to absolute amount
        watergain(soillayer+1) = watergain(soillayer+1)+change
        waterloss(soillayer) = waterloss(soillayer)+change
ENDIF

IF(waterloss(soillayer).lt.0.) THEN     ! <===== ERROR CHECK ON WATER BALANCE PROBLEM
        write(*,*)'waterloss prob'
ENDIF

RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
SUBROUTINE SOILSTOR(time,y,dydt)        !determines gravitational water drainage

USE CANOPY
USE SOIL_STRUCTURE
USE soilinfo
USE hourscale
USE integrator
IMPLICIT NONE

INTEGER i
REAL dydt(NMAX),y(NMAX)
REAL time
REAL soilcond   ! <=== FUNCTION
REAL numsecs, drainage

numsecs = 3600.*24./REAL(steps)   ! #secs per timestep

drainage = soilcond(y(1))*numsecs

IF(y(1).le.drainlayer*soilpor) THEN	! Gravitational drainage above a given percentage of porosity 
        drainage = 0.0
ELSE IF(drainage.gt.liquid) THEN	! Ice does not drain
        drainage = liquid
ELSE IF(drainage.gt.unsat) THEN		! Layer below cannot accept more water than unsat
        drainage = unsat
ENDIF

dydt(1) = -drainage                                                       ! Waterloss from this layer

RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
SUBROUTINE CANOPY_BALANCE(time)

USE SOIL_STRUCTURE
USE INTEGRATOR
USE HOURSCALE
IMPLICIT NONE

INTEGER time
INTEGER NVAR,i
PARAMETER (NVAR=2)
INTEGER kmax, kount, nbad, nok
REAL dxsav, eps, h1, hmin, x(KMAXX), y(NMAX,KMAXX), x1, x2
REAL dstore_dt, ystart(nvar)
EXTERNAL CANSTOR, RKQS
COMMON /path/ kmax, kount, dxsav, x, y

eps = 1.0e-4; h1 = .001; hmin = 0.0; kmax = 100; x1 = 1.; x2 = 2.
dxsav = (x2-x1)/15.0	! I assume this '20' is the number of soil layers (must be changed if changing SLAYERS)

! Initial conditions
IF(canopy_store.lt.1e-5*max_storage) canopy_store=0.     ! Empty store if it's tiny
ystart(1) = canopy_store  
ystart(2) = surface_watermm

CALL ODEINT(ystart,nvar,x1,x2,eps,h1,hmin,nok,nbad,CANSTOR,RKQS)

!write(*,'(/1x,a,t30,i3)')      ' Successful steps:',nok
!write(*,'(1x,a,t30,i3)')       ' Bad steps:',nbad
!write(*,'(1x,a,t30,i3)')       ' Stored intermediate values:',kount

canopy_store = ystart(1)        ! Amount of canopy storage
surface_watermm = ystart(2)     ! Amount of water reaching the soil surface

!write(20,'(7("   ",F8.2))')
RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
SUBROUTINE CANSTOR(time,y,dydt)             
! Determines canopy water storage and evaporation, and water reaching soil surface

USE CANOPY
USE SOIL_STRUCTURE
USE hourscale
USE integrator
IMPLICIT NONE

INTEGER i
REAL dydt(NMAX), y(NMAX), numsecs
REAL time
REAL wetevap ! <===== FUNCTION
REAL nummin, add_store, drain_store, add_ground, potential_evap, ratio, b, a

nummin = 60.*24./REAL(steps)                ! Minutes per timestep
add_store = (1.-through_fall)*hourppt       ! Rate of input of water to canopy storage per min
add_ground = through_fall*hourppt           ! Rate of input of water to ground
potential_evap = wetevap()                  ! Function gives potential evaporation rate
ratio = MIN(1.,y(1)/max_storage)            ! Rate of evaporation from storage NB store cannot exceed max storage
evap_store = potential_evap*ratio
b = 3.7
a = LOG(0.002)-b*max_storage

IF(y(1).gt.max_storage) THEN
    drain_store = EXP(a+b*y(1))*nummin  ! Rate of drainage from store, mm t-1
ELSE
    drain_store = 0.
ENDIF

IF((y(2).gt.0.).and.(y(2).lt.7e-37)) THEN
    WRITE(*,*)'Canopy storage problem located in [SR CANSTOR]'
    WRITE(*,*)'- Very little water reaching soil surface -'
    PAUSE
ENDIF

dydt(1) = add_store-drain_store-evap_store  ! Change in canopy storage
dydt(2) = add_ground+drain_store            ! Addition to soilwater

IF(dydt(1).gt.100.) THEN
    WRITE(*,*)'Canopy storage problem located in [SR CANSTOR]'
    PAUSE
ENDIF

RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
REAL FUNCTION WETEVAP()         ! Evaporation from wetted surfaces, mm t-1

USE hourscale
USE VEG
USE energy_constants
IMPLICIT NONE

INTEGER i
REAL z, d, z0, slope, rho, lambda, psych, s, h, ra, htemp, numsecs

h = canht                           ! Vegetation height
d=0.75*h; z0=0.1*h; z=h+2.          ! Boundary layer constants
htemp = hourtemp-freeze             ! Convert K to oC

rho = 353.0/hourtemp                                    ! Density of air g m-3 (t-dependent)
ra = (1./vonkarman**2*hourwind)*(LOG(z-d)/z0)**2        ! Aerodynamic resistance
s = 6.1078*17.269*237.3*EXP(17.269*htemp/(237.3+htemp))
slope = 100.*(s/(237.3+htemp)**2)                       ! Slope of saturation vapour pressure curve (t-dependent)
lambda = 1000.*(2501.0-2.364*htemp)                     ! Latent heat of vapourisation
psych = 100.*(0.646*EXP(0.00097*htemp))                 ! Psychrometer constant 

IF(slope*hourrnet.gt.rho*cpair*hourvpd/ra) THEN
        wetevap = (slope*hourrnet+rho*cpair*hourvpd/ra)/(lambda*(slope+psych)) ! Penman-Monteith equation, kg m-2 s-1
ELSE
        wetevap = 0.      ! Actually dewfall occurs
ENDIF
numsecs = 3600.*24./REAL(steps)   ! #secs per timestep
wetevap = wetevap*numsecs ! Convert from kg m-2 s-1 to mm t-1

IF(wetevap.lt.-1000) THEN
        WRITE(*,*)'wetevap prob'
        PAUSE
ENDIF

RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
SUBROUTINE THAWDEPTH(ts)        ! Determines layer ice fraction, and depth of thaw

USE SOIL_STRUCTURE
USE hourscale
IMPLICIT NONE

INTEGER k, liquid(0:layer), numthaw
REAL root
REAL ts, thaw(layer), topt, topthick, depthtotop(layer), middepth(layer), split
thaw = -9999.
numthaw = 1         ! There may be two or more thaw points, so thaw is an array
depthtotop = 0.     ! Records depth to start of each layer
iceprop = 0.

IF(ts.gt.freeze) THEN
        liquid(0) = 1
ELSE
        liquid(0) = 0
ENDIF

DO k=1,layer    ! Check if layer centres are frozen
        IF(soiltemp(k).gt.freeze) THEN
                liquid(k) = 1
        ELSE
                liquid(k) = 0
        ENDIF
ENDDO
DO k=1,layer    ! Locate thaw depth
        IF(k.eq.1) THEN  ! For layer 1, upper boundary is surface temperature
                topthick = 0.
                middepth(k) = 0.5*thickness(k)
                depthtotop(k) = 0.
        ELSE
                topthick = thickness(k-1)
                depthtotop(k) = SUM(depthtotop)+thickness(k-1)    ! Increment
                middepth(k) = depthtotop(k)+0.5*thickness(k)
        ENDIF
        IF(liquid(k-1).ne.liquid(k)) THEN                ! Change of state - locate thaw
                IF(k.eq.1) THEN  ! For layer 1, upper boundary is surface temperature
                        topt = ts
                ELSE
                        topt = soiltemp(k-1)
                ENDIF
                root = (freeze-soiltemp(k))*(0.5*thickness(k)+0.5*topthick)/(topt-soiltemp(k))    ! Location of thaw relative to layer midpoint
                thaw(numthaw) = middepth(k)-root  ! Determine thaw depth
                !now ice fractions
                IF(thaw(numthaw).gt.depthtotop(k)) THEN  ! Thaw is in layer k
                        split = (thaw(numthaw)-depthtotop(k))/(0.5*thickness(k))    ! Fraction of top half of layer thawed/unthawed
                        IF(soiltemp(k).lt.freeze) THEN   
                                iceprop(k) = iceprop(k)+0.5*(1.-split)
                        ELSE
                                iceprop(k) = iceprop(k)+0.5*split
                                IF(k.gt.1)iceprop(k-1) = iceprop(k-1)+0.5           ! Bottom half of k-1 is frozen
                        ENDIF
                ELSE    !thaw is in layer k-1
                        split = (depthtotop(k)-thaw(numthaw))/(0.5*thickness(k-1))  ! Fraction of bottom half of layer-1 thawed/unthawed
                        IF(soiltemp(k-1).lt.freeze)THEN         !
                                iceprop(k-1) = iceprop(k-1)+0.5*(1.-split)
                        ELSE
                                iceprop(k-1) = iceprop(k-1)+0.5*split
                                iceprop(k) = iceprop(k)+0.5       ! Top half of layer k is frozen
                        ENDIF
                ENDIF
                numthaw = numthaw+1               ! Next thaw has separate storage location
        ELSE ! No change of state
                IF(liquid(k-1)+liquid(k).eq.2) THEN     ! Both water
                        iceprop(k) = iceprop(k)
                        IF(k.gt.1)iceprop(k-1) = iceprop(k-1)
                ELSE                                    ! Both ice
                        iceprop(k) = iceprop(k)+0.5                 !*waterfrac(k)
                        IF(k.gt.1)iceprop(k-1) = iceprop(k-1)+0.5   !*waterfrac(k)
                ENDIF
        ENDIF
ENDDO
RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
REAL FUNCTION THERMCOND(i)              ! Hillel p.295
! Thermal conductivity W m-1 K-1 
USE SOIL_STRUCTURE
USE hydrol
IMPLICIT NONE

INTEGER i
REAL mintherm

! Mineral thermal conductivity depends on quarts vs other mineral content
! mintherm = 8.8*mineralfrac(i)*sandpc*0.01+2.9*mineralfrac(i)*(100.-sandpc)*0.001

! thermcond = mintherm+0.25*organicfrac(i)+0.57*(waterfrac(i)*(1.-iceprop(i)))+&
! 2.2*waterfrac(i)*iceprop(i)     
IF (i.lt.4.) THEN
   thermcond = 0.34+(0.58-0.34)*(iceprop(i))
ENDIF
IF ((i.ge.4.).and.(i.lt.10.)) THEN
   thermcond = 1.5+(2.05-1.5)*(iceprop(i))
ENDIF
IF (i.ge.10.) THEN
   thermcond = 1.69+(3.63-1.69)*(iceprop(i))
ENDIF


RETURN
END
! --------------------------------------------------------------------



! --------------------------------------------------------------------
REAL FUNCTION HEATCAP(i)                ! Walbroeck: impacts of freeze/thaw on energy fluxes
                                        ! Heat capacity, J m-3 K-1 
USE SOIL_STRUCTURE
IMPLICIT NONE

INTEGER i
REAL lhf, delt, temp, lw, volhc, p, deltaT, Thh, Tll, LHC1, LHC2, L
lhf = 334000.     ! Latent heat of fusion of ice, J kg-1
delt = 1.0        ! Temperature range over which freezing occurs
temp = soiltemp(i)-273.15         ! Convert K to oC

volhc = 2e6*mineralfrac(i)+2.5e6*organicfrac(i)+4.2e6*(waterfrac(i)*(1.-iceprop(i)))+&
                1.9e6*waterfrac(i)*iceprop(i)   ! J m-3 K-1, Hillel 1980 p. 294

IF((temp.le.0.).and.(temp.gt.(0.-delt))) THEN
        lw=  1000.*waterfrac(i)*(thickness(i)/0.1)           ! Liquid water content, kg m-3 soil
        heatcap = volhc+lhf*lw/(delt)
ELSE
        heatcap = volhc
ENDIF


RETURN
END
!---------------------------------------------------------------------



! --------------------------------------------------------------------
REAL FUNCTION ENERGY(Ts)
                                ! See Hinzmann et al. 1998.  Determines the surface soil temperature 
USE HOURSCALE
USE ENERGY_CONSTANTS
USE VEG
USE SOIL_STRUCTURE
IMPLICIT NONE

REAL rho, gah, esat, ea, esurf, ts, lambda, qe1
REAL QEFLUX, EXCO, THERMCOND      ! <=== Functions

rho = 353.0/hourtemp              ! Density of air kg m-3 (t-dependent)
gah = exco(ts)                    ! Conductance to heat, m s-1
Qh = cpair*rho*gah*(hourtemp-ts)  ! Sensible heat flux

Qe = QEFLUX(ts)                  ! Latent energy flux
!Qe = Qe1
!Qe = min(Qe1,0.)                 ! Prevent dew formation
!IF(soiltemp(1).le.freeze) Qe=0.   ! No soil evap if surface is frozen

Qn = 0.85*hourrad-emiss*boltz*ts**4                     ! Net radiation - emitted LW varies with surface temp
!Qc = -thermal*(ts-soiltemp(1))/(0.5*thickness(1))		! Soil heat flux
Qc = -thermcond(1)*(ts-soiltemp(2))/(0.5*thickness(1))  ! Hillel thermal conductivity of top layer

energy = Qh+Qe+Qn+Qc      ! Energy balance

RETURN
END
! --------------------------------------------------------------------


! --------------------------------------------------------------------
REAL FUNCTION EXCO(ts)  ! Heat or vapour exchange coefficient/boundary layer cond, m s-1
! see ref 1028          ! === DOES NOT USE SURFACE TEMPERATURE IN THE CURRENT ITERATION ===
USE HOURSCALE
USE ENERGY_CONSTANTS
USE VEG
USE SOIL_STRUCTURE
USE METEO       
IMPLICIT NONE

REAL Ri, ts, neutral, Ntemp

! Boundary layer conductance at ground level - substitute lower altitude than canht [gAH]
neutral = hourwind*vonkarman**2/(LOG(towerht/(0.13*canht)))**2    ! Heat exchange coefficient from Hinzmann, 0.13*canht gives roughness length
!neutral = hourwind*vonkarman**2/(log(towerht/(0.13*20.)))**2     ! 20.0 m altitude

exco = neutral

!Ri = grav*towerht*(hourtemp-ts)/(hourwind**2*hourtemp)     ! Richardson number from Hinzman
!Ntemp = hourtemp+0.01*towerht                              ! Ground surface temperature for neutrality, vertical temp gradient = 0.01C m-1, Grace, p.46

!IF(ts.eq.Ntemp) THEN               ! Neutral conditions
!       exco = neutral              ! Heat exchange coefficient from Hinzmann, 0.13*canht gives roughness length
!ELSE IF(ts.gt.Ntemp) THEN          ! Unstable conditions
!       exco = neutral*(1.-10.*Ri)
!ELSE           ! Stable conditions
!       exco = neutral/(1.+10.*Ri)
!ENDIF


RETURN
END
! --------------------------------------------------------------------


! --------------------------------------------------------------------
REAL FUNCTION QEFLUX(ts)        ! Latent energy loss from soil surface

USE HOURSCALE
USE ENERGY_CONSTANTS
USE VEG
USE SOIL_STRUCTURE
USE METEO       
USE CLIM
IMPLICIT NONE

REAL ts, lambda, rho, esat, ea, esurf, diff, por, tort
REAL exco                 ! <=== Function

!por = 0.5; tort = 2.     ! Porosity and tortuosity
tort = 2.5                ! Tortuosity
por = porosity(1)         ! Porosity of surface layer

lambda = 1000.*(2501.0-2.364*(ts-273.15))         ! Latent heat of vapourisation, J kg-1
gaw = EXCO(ts)                    ! Determine boundary layer conductance, m s-1
rho = 353.0/hourtemp              ! Density of air kg m-3 (t-dependent)
diff = 24.2e-6*(ts/293.2)**1.75   ! m2 s-1 diffusion coefficient of water vapour in air

esat = 0.1*EXP(1.80956664+(17.2693882*hourtemp-4717.306081)/(hourtemp-35.86))     ! kPa, saturation vapour pressure of air
ea = esat-hourvpd                 ! Vapour pressure of air
 
esat = 0.1*EXP(1.80956664+(17.2693882*ts-4717.306081)/(ts-35.86))         ! kPa, saturation vapour pressure at surface - assume saturation
esurf = esat*EXP(1e6*SWP(1)*Vw/(Rcon*Ts))         ! kPa, vapour pressure in soil airspace, dependent on soil water potential - Jones p.110. Vw=partial molal volume of water
gws = por*diff/(tort*drythick)                    ! Soil conductance to water vapour diffusion, m s-1


Qeflux = lambda*rho*0.622/(0.001*hourpress)*(ea-esurf)/(1./gaw+1./gws)

IF(ts.lt.freeze) QEflux=0.       ! No evap if surface is frozen

RETURN
END
! --------------------------------------------------------------------


! --------------------------------------------------------------------
SUBROUTINE WATERTHERMAL(time)	! Redistribute heat according to water movement 
                                ! in the soil 
USE HYDROL
USE SOIL_STRUCTURE
USE HOURSCALE
USE CLIM	
IMPLICIT NONE

INTEGER i, time
REAL heat, heatloss, ttemp, newheat, heatgain, volhc
REAL watercontent, icecontent, numsecs, evap, lambda
numsecs = 3600.*24./REAL(steps)	! # of seconds per timestep

DO i=1,layer	
	volhc = 2e6*mineralfrac(i)+2.5e6*organicfrac(i)+4.2e6*(waterfrac(i)*(1.-iceprop(i)))+&
		1.9e6*waterfrac(i)*iceprop(i)	        ! Volumetric heat capacity of soil layer, J m-3 K-1
	heat = soiltemp(i)*volhc*thickness(i)		! Heat content of the layer, J m-2
	heatloss = waterloss(i)*4.2e6*soiltemp(i) 	! Water loss in m, * heat capacity of water * temp = J m-2
	heatgain = watergain(i)*4.2e6*soiltemp(i)+pptgain(i)*4.2e6*hourtemp 	    ! Water gain in m, * heat capacity of water * temp = J m-2
	
	watercontent = (waterfrac(i)*(1.-iceprop(i)))*thickness(i)	                ! Liquid t or m of water
	icecontent = (waterfrac(i)*iceprop(i))*thickness(i)			                ! Solid t or m of water
	watercontent = MAX(0.,watercontent+watergain(i)+pptgain(i)-waterloss(i))	! Now shift water content
	waterfrac(i) = (watercontent+icecontent)/thickness(i)
	iceprop(i) = icecontent/(watercontent+icecontent)
	IF(waterfrac(i).gt.porosity(i)*1.1) THEN
		WRITE(*,*) 'overflow'
	ENDIF
	IF(heatgain+heatloss.ne.0.) THEN		        ! Net redistribution of heat
		newheat = heat-heatloss+heatgain
		volhc = 2e6*mineralfrac(i)+2.5e6*organicfrac(i)+4.2e6*(waterfrac(i)*(1.-iceprop(i)))+&
			1.9e6*waterfrac(i)*iceprop(i)	! Volumetric heat capacity of soil layer, J m-3 K-1
		soiltemp(i) = newheat/(volhc*thickness(i))
	ENDIF
	watericemm(i) = 1e3*waterfrac(i)*thickness(i)	! mm of water in layer
ENDDO
lambda = 1000.*(2501.0-2.364*(hourts-273.15))	    ! Latent heat of vapourisation, J kg-1
evap = 0.001*(-Qe/lambda)*numsecs	! m t-1
!WRITE(63,'(30(F9.4,","))')daytime(time),(waterfrac(i)*thickness(i),i=1,layer),evap
!WRITE(63,'(30(F9.4,","))')daytime(time), waterfrac(i),i=1,layer),evap*1e3
dayevap = -Qe	! W m-2
underflow = watergain(core)
discharge = discharge+watergain(core)*1e3


RETURN
END
! --------------------------------------------------------------------


! --------------------------------------------------------------------
SUBROUTINE WATERFLUXES(time)            ! Waterfrac is m3 m-3, soilwp is MPa
                                        ! Conductivity corrected 12 Feb 1999
USE HYDROL
USE SOIL_STRUCTURE
USE hourscale                           ! Contains soil evap estimate Qe
USE CLIM
USE METEO
IMPLICIT NONE

INTEGER i, rr, ar1(1), ar2, ar1b, time
REAL soil_WP, soil_sat, maxloss, unsat, rs, Lsoil, rs2, pi, maxdrain, diff, netc
REAL potA, potB, cond1, cond2, cond3, numsecs, lambda, watercontent, airspace
REAL A, B, CC, D, E, F, G, H, J, K, P, Q, R, T, U, V, mult1, mult2, mult3, nextwf1

mult1=100.;  mult2=2.778e-6; mult3=1000.0;  
A=-4.396; B=-0.0715; CC=-4.880e-4; D=-4.285e-5; E=-3.140; F=-2.22e-3;  G=-3.484e-5; H=0.332
J=-7.251e-4; K=0.1276; P=12.012; Q=-7.551e-2; R=-3.895; T=3.671e-2; U=-0.1103; V=8.7546e-4
snow =0

numsecs = 3600.*24./REAL(steps)                 ! # of seconds per timestep
pi = 3.14159
lambda = 1000.*(2501.0-2.364*(hourts-freeze))   ! Latent heat of vapourisation, J kg-1

CALL WETTINGLAYERS(time)
IF(drythick.lt.thickness(1)) THEN        ! From which layer is evap water withdrawn?
	rr = 1            ! The dry zone does not extend beneath the top layer
ELSE
	rr = 2            ! It does
ENDIF
IF(Qe.lt.0.) THEN    ! Evaporation
	waterloss(rr) = waterloss(rr)+0.001*(-Qe/lambda)*numsecs  ! t m-2 t-1, m t-1
ELSE    ! Dew formation
	watergain(1) = watergain(1)+0.001*(Qe/lambda)*numsecs     ! t m-2 t-1, m t-1
ENDIF

totet = MAX(0.,totet)
DO i=1,rootl            ! Water loss from each layer 
	waterloss(i) = waterloss(i)+totet*fraction_uptake(i)*abovebelow
ENDDO
DO i=1,layer
	CALL SOIL_BALANCE(i)
	IF(waterloss(i).lt.0.) THEN
		write(*,*)'trouble with water loss = ',waterloss(i),' in layer ',i
	ENDIF
ENDDO
CALL INFILTRATE()

CALL SWPSOILR()                 ! Find SWP & soil resistance without updating waterfrac yet (do that in waterthermal)

IF(time.eq.1) THEN
	unintercepted = surface_watermm
ELSE
	unintercepted = unintercepted+surface_watermm     ! Determine total water reaching ground for day summation
ENDIF

RETURN
END
! --------------------------------------------------------------------


! --------------------------------------------------------------------
SUBROUTINE INFILTRATE()         ! Takes surface_watermm and distrubutes it among top layers
                                ! - assumes total infilatration in timestep
USE HYDROL
USE SOIL_STRUCTURE
USE hourscale                   ! Contains soil evap estimate Qe
USE CLIM
USE METEO
IMPLICIT NONE

INTEGER i
REAL wdiff, add

add = surface_watermm*0.001
DO i=1,layer
	wdiff = MAX(0.,(porosity(i)-waterfrac(i))*thickness(i)-watergain(i)+waterloss(i))
	IF(add.gt.wdiff) THEN
		pptgain(i) = wdiff
		add = add-wdiff
	ELSE
		pptgain(i) = add
		add = 0.
	ENDIF
	IF(add.le.0.) EXIT
ENDDO
IF(add.gt.0.) THEN
	overflow = add
ELSE
	overflow = 0.
ENDIF
runoff = runoff+overflow

RETURN
END
! --------------------------------------------------------------------


! --------------------------------------------------------------------
SUBROUTINE WETTINGLAYERS(time)
                        ! Surface wetting and drying determines thickness of dry layer and thus Qe
USE HYDROL
USE SOIL_STRUCTURE
USE hourscale           ! Contains soil evap estimate Qe
USE CLIM
USE METEO
IMPLICIT NONE

INTEGER i, rr, ar1(1), ar2, ar1b, time
REAL airspace, diff, netc, lambda, numsecs, dmin
!dmin = 0.00001
dmin = 0.001

numsecs = 3600.*24./REAL(steps)                     ! # of seconds per timestep
lambda = 1000.*(2501.0-2.364*(hourtemp-freeze))     ! Latent heat of vapourisation, J kg-1
! airspace = 1.-(organicfrac(1)+mineralfrac(1))
airspace = porosity(1)

! From which wetting layer should soil LE be withdrawn? The one with the smallest depth
ar1 = MINLOC(wettingbot,MASK = wettingbot.gt.0.)
ar1b = SUM(ar1)   ! Convert ar1 to scalar

! What is the net change in wetting in the top zone? <--- delta_l (p12)
netc = (0.001*Qe/lambda*numsecs)/airspace+(surface_watermm*0.001+snow)/airspace   ! m

! ====== WETTING ======
IF(netc.gt.0.) THEN      
    IF((netc.gt.wettingtop(ar1b)).and.(wettingtop(ar1b).gt.0.)) THEN         ! Resaturate the layer if top is dry and recharge is greater than drythick
        diff = netc-wettingtop(ar1b)        ! Extra water to deepen wetting layer
        wettingtop(ar1b) = 0.
        IF(ar1b.gt.1) THEN                  ! Not in primary layer (primary layer can't extend deeper)
            wettingbot(ar1b) = wettingbot(ar1b)+diff
        ENDIF
        drythick = dmin
    ELSE
        IF(wettingtop(ar1b).eq.0.) THEN     ! Surface is already wet, so extend depth of this wet zone
            IF(ar1b.gt.1) THEN              ! Not in primary layer (primary layer can't extend deeper)
                wettingbot(ar1b) = wettingbot(ar1b)+netc          
                IF(wettingbot(ar1b).ge.wettingtop(ar1b-1)) THEN  ! Layers are conterminous
                    wettingtop(ar1b-1) = wettingtop(ar1b)
                    wettingtop(ar1b) = 0.   ! Remove layer
                    wettingbot(ar1b) = 0.
                ENDIF
            ENDIF
        ELSE    ! Or create a new wetting zone
            wettingtop(ar1b+1) = 0.
            wettingbot(ar1b+1) = netc
        ENDIF
        drythick = dmin
    ENDIF
! ====== DRYING ======
ELSE    
    wettingtop(ar1b) = wettingtop(ar1b)-netc            ! Drying increases the wettingtop depth
    IF(wettingtop(ar1b).ge.wettingbot(ar1b)) THEN       ! Wetting layer is dried out
        diff = wettingtop(ar1b)-wettingbot(ar1b)        ! How much more drying is there?                 
        wettingtop(ar1b) = 0.
        wettingbot(ar1b) = 0.
        ar2 = ar1b-1
        IF(ar2.gt.0) THEN        ! Move to deeper wetting layer
            wettingtop(ar2) = wettingtop(ar2)+diff    ! Dry out deeper layer
            drythick = MAX(dmin,wettingtop(ar2))
        ELSE    ! No deeper layer
            drythick = thickness(1)   ! Layer 1 is dry
        ENDIF
    ELSE
        drythick = MAX(dmin,wettingtop(ar1b))
    ENDIF
ENDIF

IF(drythick.eq.0.) THEN
    WRITE(*,*)'drythick prob'
    PAUSE
ENDIF
!WRITE(93,'(20(F12.6,"   "))')daytime(time),drythick*1e3,wettingtop(1)*1e3,wettingbot(1)*1e3,wettingtop(2)*1e3,wettingbot(2)*1e3,netc*1e3

RETURN
END
!---------------------------------------------------------------------------------------


! ======================================================================================
! =========================== BELOW ARE THE MAJOR SNOW ROUTINES ========================
! ======================================================================================


!---------------------------------------------------------------------------------------
SUBROUTINE SNOWRUN()

USE CLIM
USE HOURSCALE
USE SOIL_STRUCTURE
USE IRRADIANCE_SUNSHADE
USE VEG
USE SNOWINFO
IMPLICIT NONE

REAL CV_old(3),snowheat_old,snowweight_old,snowheight_old,Tsnow_old(3),ro_snow_old(3)
REAL f_I_old(3),snowht_old(3),snowh_old(3),snoww_old(3),W_L_old(3)
REAL CV1(3),snowheat1,snowweight1,snowheight1,Tsnow1(3),ro_snow1(3)
REAL f_I1(3),snowht1(3),snowh1(3),snoww1(3),W_L1(3)
REAL CV2(3),snowheat2,snowweight2,snowheight2,Tsnow2(3),ro_snow2(3)
REAL f_I2(3),snowht2(3),snowh2(3),snoww2(3),W_L2(3)
REAL Hrain,Hsnow,dZ,dW,g,ro_old(3),Cv(3),dt, ro_ice, tottijd
 
!calculate new snowheight and heat content after snowfall
ro_ice=920.	!density of ice
if (hourtemp.gt.273.15) then	!rain falls
  Hrain=1014*1000.*(hourppt/1000.)*(hourtemp-273.15)
  Hsnow=0.
  dZ=0.	!no added snow height
  dW=0.	!no added snow weight
else	!snow falls
  Hrain=0.
  dW=(hourppt/1000.)
  dZ=(1000./150)*dW
  Hsnow=((hourtemp-273.15)*1.9e6*150/ro_ice-333000.*150.)*dZ
endif

if (snowweight.eq.0.) then	!all new snow
    delta_t=0.
    snowheight=snowheight+dZ
    snowweight=snowweight+dW
    Tsnow(1)=hourtemp	!temperature of snow set to air temp
    Tsnow(2)=hourtemp
    Tsnow(3)=hourtemp
    ro_snow(1)=150.		!fresh snow density
    ro_snow(2)=150.		!fresh snow
    ro_snow(3)=150.		!fresh snow
    Cv(1)=1.9e6*ro_snow(1)/ro_ice	!thermal conductivity of snow layer is dependent on packing
    Cv(2)=1.9e6*ro_snow(2)/ro_ice
    Cv(3)=1.9e6*ro_snow(3)/ro_ice
    if (snowheight.le.0.2) then	!distribute snow among layers
       snowh(1)=0.25*snowheight
       snowh(2)=0.5*snowheight
       snowh(3)=0.25*snowheight
    else
       snowh(1)=0.05
       snowh(2)=0.34*(snowheight-0.05)
       snowh(3)=0.66*(snowheight-0.05)
    endif
    snoww(1)=ro_snow(1)*snowh(1)/1000.	!snow weight
    snoww(2)=ro_snow(2)*snowh(2)/1000.
    snoww(3)=ro_snow(3)*snowh(3)/1000.
    snowht(1)=(Tsnow(1)-273.15)*Cv(1)*snowh(1)-1000*333000*snoww(1)	!snow heat content
    snowht(2)=(Tsnow(2)-273.15)*Cv(2)*snowh(2)-1000*333000*snoww(2)
    snowht(3)=(Tsnow(3)-273.15)*Cv(3)*snowh(3)-1000*333000*snoww(3)
else	!increment current snow - add new snow to top layer
    snowheight=snowheight+dZ
    snowweight=snowweight+dW
    g=9.81	!gravity for packing
    ro_snow(1)=(snowh(1)*ro_snow(1)+dZ*150.)/(snowh(1)+dZ)
    ro_old(1)=ro_snow(1)
    ro_old(2)=ro_snow(2)
    ro_old(3)=ro_snow(3)
	!now adjust density of all snow layers - gravitational packing
    ro_snow(1)=ro_old(1)+(0.5e-7*ro_old(1)*g*0.5*snoww(1)*(exp(14.643-4000./(min(Tsnow(1),273.16))&
         -0.02*ro_old(1))))*delta_t*100
    snowh(1)=ro_old(1)/ro_snow(1)*(snowh(1)+dZ)
    ro_snow(2)=ro_old(2)+(0.5e-7*ro_old(2)*g*(snoww(1)+0.5*snoww(2))*&
        (exp(14.643-4000./(min(Tsnow(2),273.16))-0.02*ro_old(2))))*delta_t*100
    snowh(2)=ro_old(2)/ro_snow(2)*snowh(2)
    ro_snow(3)=ro_old(3)+(0.5e-7*ro_old(3)*g*(snoww(1)+snoww(2)+0.5*snoww(3))*&
               (exp(14.643-4000./(min(Tsnow(3),273.16))-0.02*ro_old(3))))*delta_t*100
    snowh(3)=ro_old(3)/ro_snow(3)*snowh(3)
    snowheight=snowh(1)+snowh(2)+snowh(3)
    Cv(1)=1.9e6*ro_snow(1)/ro_ice
    Cv(2)=1.9e6*ro_snow(2)/ro_ice
    Cv(3)=1.9e6*ro_snow(3)/ro_ice
    !layers:
    if (snowheight.le.0.2) then
       snowh(1)=0.25*snowheight
       snowh(2)=0.5*snowheight
       snowh(3)=0.25*snowheight
    else
       snowh(1)=0.05
       snowh(2)=0.34*(snowheight-0.05)
       snowh(3)=0.66*(snowheight-0.05)
    endif
    snoww(1)=ro_snow(1)*snowh(1)/1000.
    snoww(2)=ro_snow(2)*snowh(2)/1000.
    snoww(3)=ro_snow(3)*snowh(3)/1000.
    snowht(1)=(Tsnow(1)-273.15)*Cv(1)*snowh(1)-1000*333000*snoww(1)+Hrain+Hsnow
    snowht(2)=(Tsnow(2)-273.15)*Cv(2)*snowh(2)-1000*333000*snoww(2)
    snowht(3)=(Tsnow(3)-273.15)*Cv(3)*snowh(3)-1000*333000*snoww(3)
endif
if (snowheight.gt.0.006) then	!if there is enough snow for stable calculations
   dt=24./real(steps)*3600.	!number of seconds per time step
   tottijd=0	!accumulated time
   totsoilflux=0.
   totwaterflux=0.
   do while (tottijd.lt.dt)
      if (snowheight.gt.0.006) then
         f_I_old=f_I	!f_I = fraction of snow that is melted
         snowht_old=snowht
         snoww_old=snoww
         snowh_old=snowh
         W_L_old=W_L   !weight of melted water
         snowheat_old=snowheat
         snowweight_old=snowweight
         snowheight_old=snowheight
         Tsnow_old=Tsnow
         ro_snow_old=ro_snow
         Cv_old=Cv
         call snowcalc(dt);	!use maximum time step
         f_I1=f_I
         snowht1=snowht
         snoww1=snoww
         snowh1=snowh
         W_L1=W_L   
         snowheat1=snowheat
         snowweight1=snowweight
         snowheight1=snowheight
         Tsnow1=Tsnow
         ro_snow1=ro_snow
         Cv1=Cv
         if (snowheight1.le.0.2) then
            snowh(1)=0.25*snowheight1
            snowh(2)=0.5*snowheight1
            snowh(3)=0.25*snowheight1
         else
            snowh(1)=0.05
            snowh(2)=0.34*(snowheight1-0.05)
            snowh(3)=0.66*(snowheight1-0.05)
         endif
         snoww(1)=ro_snow(1)*snowh(1)/1000.
         snoww(2)=ro_snow(2)*snowh(2)/1000.
         snoww(3)=ro_snow(3)*snowh(3)/1000.
         snowht(1)=(Tsnow(1)-273.15)*Cv(1)*snowh(1)-1000*333000*snoww(1)
         snowht(2)=(Tsnow(2)-273.15)*Cv(2)*snowh(2)-1000*333000*snoww(2)
         snowht(3)=(Tsnow(3)-273.15)*Cv(3)*snowh(3)-1000*333000*snoww(3)
         if (snowheight.gt.0.006) then
            call snowcalc(dt);	!run for another time step, second estimate
            f_I2=f_I
            snowht2=snowht
            snoww2=snoww
            snowh2=snowh
            W_L2=W_L   
            snowheat2=snowheat
            snowweight2=snowweight
            snowheight2=snowheight
            Tsnow2=Tsnow
            ro_snow2=ro_snow
            Cv2=Cv
			!compare first and second estimates to test for non-linear behaviour/stability
            if (((abs(snowht2(1)-snowht1(1)).gt.abs(0.01*snowht1(1))).or.&
               (abs(snowht2(1)-snowht1(1)).gt.1e7)).or.&
               ((abs(snowht2(2)-snowht1(2)).gt.abs(0.01*snowht1(2))).or.&
               (abs(snowht2(2)-snowht1(2)).gt.1e7)).or.&
               ((abs(snowht2(3)-snowht1(3)).gt.abs(0.01*snowht1(3))).or.&
               (abs(snowht2(3)-snowht1(3)).gt.1e7))) then
               if (dt.gt.0.5) then  !if there is instability, half the timestep
                   dt=dt/2.
                   f_I=f_I_old
                   snowht=snowht_old
                   snoww=snoww_old
                   snowh=snowh_old
                   W_L=W_L_old
                   snowheat=snowheat_old
                   snowweight=snowweight_old
                   snowheight=snowheight_old
                   Tsnow=Tsnow_old
                   ro_snow=ro_snow_old
                   Cv=Cv_old
               else	!otherwise, it's already so small, just use predictions from first run
                   f_I=f_I1
                   snowht=snowht1
                   snoww=snoww1
                   snowh=snowh1
                   W_L=W_L1 
                   snowheat=snowheat1
                   snowweight=snowweight1
                   snowheight=snowheight1
                   Tsnow=Tsnow1
                   ro_snow=ro_snow1
                   Cv=Cv1
                   if (snowheight.le.0.2) then
                       snowh(1)=0.25*snowheight
                       snowh(2)=0.5*snowheight
                       snowh(3)=0.25*snowheight
                   else
                       snowh(1)=0.05
                       snowh(2)=0.34*(snowheight-0.05)
                       snowh(3)=0.66*(snowheight-0.05)
                   endif
                   snoww(1)=ro_snow(1)*snowh(1)/1000.
                   snoww(2)=ro_snow(2)*snowh(2)/1000.
                   snoww(3)=ro_snow(3)*snowh(3)/1000.
                   snowht(1)=(Tsnow(1)-273.15)*Cv(1)*snowh(1)-1000*333000*snoww(1)
                   snowht(2)=(Tsnow(2)-273.15)*Cv(2)*snowh(2)-1000*333000*snoww(2)
                   snowht(3)=(Tsnow(3)-273.15)*Cv(3)*snowh(3)-1000*333000*snoww(3)
                   tottijd=tottijd+dt 
                   totsoilflux=totsoilflux+soilflux
                   if (W_L(3).gt.0.) then
                       W_L(3)=W_L(3)            
                   endif
                   totwaterflux=totwaterflux+waterflux(3)
               endif
            else	!differences between 2 runs are small, so use output from first run
               f_I=f_I1
               snowht=snowht1
               snoww=snoww1
               snowh=snowh1
               W_L=W_L1 
               snowheat=snowheat1
               snowweight=snowweight1
               snowheight=snowheight1
               Tsnow=Tsnow1
               ro_snow=ro_snow1
               Cv=Cv1
               if (snowheight.le.0.2) then
                   snowh(1)=0.25*snowheight
                   snowh(2)=0.5*snowheight
                   snowh(3)=0.25*snowheight
               else
                  snowh(1)=0.05
                  snowh(2)=0.34*(snowheight-0.05)
                  snowh(3)=0.66*(snowheight-0.05)
               endif
               snoww(1)=ro_snow(1)*snowh(1)/1000.
               snoww(2)=ro_snow(2)*snowh(2)/1000.
               snoww(3)=ro_snow(3)*snowh(3)/1000.
               snowht(1)=(Tsnow(1)-273.15)*Cv(1)*snowh(1)-1000*333000*snoww(1)
               snowht(2)=(Tsnow(2)-273.15)*Cv(2)*snowh(2)-1000*333000*snoww(2)
               snowht(3)=(Tsnow(3)-273.15)*Cv(3)*snowh(3)-1000*333000*snoww(3)
               tottijd=tottijd+dt  
               totsoilflux=totsoilflux+soilflux
               totwaterflux=totwaterflux+waterflux(3)
            endif
         else !if snowpack is too thin, then time step was too long, so shorten it some more
            if (dt.gt.0.5) then
                dt=dt/2
                f_I=f_I_old
                snowht=snowht_old
                snoww=snoww_old
                snowh=snowh_old
                W_L=W_L_old   
                snowheat=snowheat_old
                snowweight=snowweight_old
                snowheight=snowheight_old
                Tsnow=Tsnow_old
                ro_snow=ro_snow_old
                Cv=Cv_old
            else 
                if (hourtemp.gt.274) then
                   totwaterflux=totwaterflux+snowweight
                   snowht=0.
                   snoww=0.
                   snowh=0.
                   snowheat=0
                   snowweight=0
                   snowheight=0
                   W_L=0 
                   totsoilflux=0. 
                else
                   f_I=f_I1
                   snowht=snowht1
                   snoww=snoww1
                   snowh=snowh1
                   W_L=W_L1 
                   snowheat=snowheat1
                   snowweight=snowweight1
                   snowheight=snowheight1
                   Tsnow=hourtemp
                   totsoilflux=totsoilflux+0.
                   ro_snow=ro_snow1
                   Cv=Cv1
                   if (snowheight.le.0.2) then
                      snowh(1)=0.25*snowheight
                      snowh(2)=0.5*snowheight
                      snowh(3)=0.25*snowheight
                   else
                      snowh(1)=0.05
                      snowh(2)=0.34*(snowheight-0.05)
                      snowh(3)=0.66*(snowheight-0.05)
                   endif
                   snoww(1)=ro_snow(1)*snowh(1)/1000.
                   snoww(2)=ro_snow(2)*snowh(2)/1000.
                   snoww(3)=ro_snow(3)*snowh(3)/1000.
                   snowht(1)=(Tsnow(1)-273.15)*Cv(1)*snowh(1)-1000*333000*snoww(1)
                   snowht(2)=(Tsnow(2)-273.15)*Cv(2)*snowh(2)-1000*333000*snoww(2)
                   snowht(3)=(Tsnow(3)-273.15)*Cv(3)*snowh(3)-1000*333000*snoww(3)
                   tottijd=tottijd+dt  
                   totsoilflux=totsoilflux+0
                   totwaterflux=totwaterflux+0
                endif
            endif
         endif         
      else	!thin snow, get rid of it all if it is warm enough, else, leave it alone
         if (hourtemp.gt.274) then
            totwaterflux=totwaterflux+snowweight
            snowht=0.
            snoww=0.
            snowh=0.
            snowheat=0
            snowweight=0
            snowheight=0
            W_L=0 
            Tsnow=273.15
            totsoilflux=0.
         else
            Tsnow=hourtemp
         endif
         tottijd=tottijd+dt
      endif
    enddo
    delta_t=delta_t+tottijd
else  !thin snow, get rid of it all if it is warm enough, else, leave it alone
   if (hourtemp.gt.274) then
         totwaterflux=snowweight
         snowht=0.
         snoww=0.
         snowh=0.
         snowheat=0
         snowweight=0
         snowheight=0
         W_L=0 
         Tsnow=273.15
         totsoilflux=0.
         delta_t=0.
   else
         Tsnow=hourtemp
         totsoilflux=0.
         totwaterflux=0.
   endif
endif
RETURN
END

!-----------------------------------------------------------
subroutine SNOWCALC(dt)
! snow energy balance calculations, sublimation, evaporation and heat flux to soil
USE CLIM
USE HOURSCALE
USE SOIL_STRUCTURE
USE IRRADIANCE_SUNSHADE
USE VEG
USE SNOWINFO
real rho,ro_ice,cpair,emiss,boltz,Cw,K_therm(3),Cv(3),gah
real Hsens,lambda,diff,esat,ea,Hlat,Hrain,Hsnow,dZ,dW,ro_old(3),Hmelt(3)
real buf1,buf2
real alb, rad,H0,E0,Bol,F_Iold(3)
real soilcond,dt,earel1,earel2,W_L_old(3),bla,thermflux(3),fr,W_test
integer i

alb=0.45	!albedo of snow
L_fu=333000	!latent heat of fusion
ro_w=1000	!density of water
!rho=353.0/(hourtemp)
ro_ice=920.	!density of ice
cpair=1012.	!specific heat capac of air
emiss=0.96	!emissivity
boltz=5.6703e-8	!botlzmann
Cw=4.1868	!specific heat capac of water
!thermal conductivities of snow layers
K_therm(1)=3.2217e-6*(ro_snow(1)*ro_snow(1))
K_therm(2)=3.2217e-6*(ro_snow(2)*ro_snow(2))
K_therm(3)=3.2217e-6*(ro_snow(3)*ro_snow(3))
do i=1,3	!step through the 3 layers
   ro_snow(i)=1000*((snoww(i))/snowh(i))
   Cv(i)=1.9e6*(ro_snow(i))/ro_ice
   Tsnow(i)=(snowht(i)+ro_w*L_fu*snoww(i))/(Cv(i)*snowh(i))+273.15
   if (Tsnow(i).le.40.)then	!dirty stabiliser
       Tsnow(i)=40.
   endif
   C_h=1.2e-3
   C_e=1.e-3
   H0=2.0
   E0=2.0
   if (i.eq.1) then	!evaporation/sublimation from layer 1 only
      rad=hourrad*(1.-alb)*dt
      !sensible heat flux:
      Hsens=-((H0+(hourpress)/(8.31*hourtemp)*cpair*C_h*hourwind)*(Tsnow(i)-hourtemp))*dt
      !latent heat flux:
      esat=0.1*exp(1.80956664+(17.2693882*(hourtemp)-4717.306081)/(hourtemp-35.86))
      if (hourvpd.lt.0.) then
          hourvpd=0.
      endif
      earel1=1.-hourvpd/esat
      esat=0.1*exp(1.80956664+(17.2693882*Tsnow(i)-4717.306081)/(Tsnow(i)-35.86))
      earel2=1.
      Hlat=(hourpress*1e-3/0.622*(E0+100*L_fu/(8.31*hourtemp)*C_e*hourwind)*(earel2-earel1))*dt
      if (Hlat.lt.0.) then	!snow forms from water vapour at surface, increase snow layer height and weight
          snoww(i)=snoww(i)-(Hlat/2.835e9)
          snowh(i)=1000*(snoww(i)/ro_snow(i))
          Hlat=0.
      else	!snow loss via evaporation of liquid water first, and then sublimation of snow
         W_L_old(i)=W_L(i)
         W_test=W_L_old(i)
         W_test=W_test-(Hlat/(2.5e6-2373*273.15))/1000.
         if (W_test.lt.0.) then	!liquid water is present
             W_test=0.
             snoww(i)=snoww(i)-W_L_old(i)
             if (snoww(i).lt.0.) then
                  snoww(i)=0.
             endif
             W_L(i)=W_L(i)-W_L_old(i)
             ro_snow(i)=1000*((snoww(i))/snowh(i))
             snowh(i)=1000*(snoww(i)/ro_snow(i))
             if (W_L_old(i).ge.0.) then
                Hlat=Hlat*(W_L_old(i)/(Hlat/(2.5e6-2373*273.15)/1000.))
                 if (snoww(i).lt.0.) then
                     snoww(i)=0.
                 endif
                 snowh(i)=1000*(snoww(i)/ro_snow(i)) 
             else
                 if (snowh(i).lt.0.) then
                     snowh(i)=0.
                 endif
             endif
         else	!no liquid water, so all energy is used for sublimation
             if (W_L_old(i).gt.0.) then
                 W_L(i)=W_L(i)-(Hlat/(2.5e6-2373*273.15))/1000.
                 snoww(i)=snoww(i)-(Hlat/(2.5e6-2373.*273.15))/1000.
                 if (snoww(i).lt.0.) then
                      snoww(i)=0.
                      snowh(i)=0.
                 else
                    ro_snow(i)=1000*((snoww(i))/snowh(i))
                 endif
                 snowh(i)=1000*(snoww(i)/ro_snow(i)) 
             endif
         endif
      endif
      !Boltzman:
      Bol=emiss*boltz*(Tsnow(1))**4*dt
      !Incoming longwave
      long=0.001*boltz*(hourtemp-20.)**4*dt		!should this be (hourtemp-20)?
   endif
   
   if (i.eq.3) then	!thermal flux: from layer 3 to soil
      soilcond=thermcond(1)
      soilflux=(-((snowh(i)+thickness(1))/(snowh(i)/K_therm(i)+thickness(1)/(soilcond)))&
              *((Tsnow(i)-soiltemp(1))/(snowh(i)/2+thickness(1)/2)))*dt
   else	!thermal fluxes for layer 1 and 2
      thermflux(i)=(-((snowh(i)+snowh(i+1))/(snowh(i)/K_therm(i)+snowh(i+1)/(K_therm(i+1))))&
               *((Tsnow(i)-Tsnow(i+1))/(snowh(i)/2+snowh(i+1)/2)))*dt
   endif
   !energy balance of each snow layer
   if (i.eq.1) then
       snowht(i)=snowht(i)+rad+thermflux(i)+Hsens-Hlat-Bol+long
       if (snowh(i).gt.0.) then
         Cv(i)=1.9e6*(ro_snow(i))/ro_ice
          Tsnow(i)=(snowht(i)+ro_w*L_fu*snoww(i))/(Cv(i)*snowh(i))+273.15
          if (Tsnow(i).le.40.)then
              Tsnow(i)=40.
          endif
       else
          Tsnow=273.15
       endif
   endif
   if (i.eq.2) then
      snowht(i)=snowht(i)+thermflux(i)-thermflux(i-1)
       if (snowh(i).gt.0.) then
         Cv(i)=1.9e6*(ro_snow(i))/ro_ice
          Tsnow(i)=(snowht(i)+ro_w*L_fu*snoww(i))/(Cv(i)*snowh(i))+273.15
          if (Tsnow(i).le.40.)then
              Tsnow(i)=40.
          endif
       else
          Tsnow=273.15
       endif
      snoww(i)=snoww(i)+waterflux(i-1)
      ro_snow(i)=1000*((snoww(i))/snowh(i))
      W_L(i)=W_L(i)+waterflux(i-1)
   endif
   if (i.eq.3) then
      snowht(i)=snowht(i)+soilflux-thermflux(i-1)
       if (snowh(i).gt.0.) then
         Cv(i)=1.9e6*(ro_snow(i))/ro_ice
          Tsnow(i)=(snowht(i)+ro_w*L_fu*snoww(i))/(Cv(i)*snowh(i))+273.15
          if (Tsnow(i).le.40.)then
              Tsnow(i)=40.
          endif
       else
          Tsnow=273.15
       endif
      snoww(i)=snoww(i)+waterflux(i-1)
      ro_snow(i)=1000*((snoww(i))/snowh(i))
      W_L(i)=W_L(i)+waterflux(i-1)
   endif
   !heat and water content of snow
   if (snoww(i).gt.0.) then
      Hmelt(i)=0.
      f_Iold(i)=f_I(i)
      if ((1000.*333000.*(snoww(i))+snowht(i)).ge.0.) then	!is there an excess of heat? melt snow
         Tsnow(i)=273.15
         f_I(i)=-snowht(i)/(1000.*333000.*((snoww(i)+W_L(i))))  !calculate melted fraction
         if (f_I(i).lt.0.) then
             f_I(i)=0.
         endif
         if (f_I(i).gt.1.) then
             f_I(i)=1.
         endif
      endif
      if ((1000.*333000.*(snoww(i))+snowht(i)).lt.0.) then	!freezing of meltwater
         if ((W_L(i).gt.0.)) then	!is there any melt water to be frozen?
             fr=-(snowht(i)+1000*333000*(snoww(i)))/(1000*333000)	!freezing fraction
             if (fr.gt.W_L(i)) then
                  snowht(i)=snowht(i)+(W_L(i)*1000*333000)
                  Cv(i)=1.9e6*(ro_snow(i))/ro_ice
                  Tsnow(i)=(snowht(i)+1000.*333000.*snoww(i))/(Cv(i)*snowh(i))+273.15
                  W_L(i)=0.
             else
                  W_L(i)=W_L(i)-fr
                  snowht(i)=snowht(i)+(fr*1000*333000)
                  Tsnow=273.15
             endif
         endif
         if (Tsnow(i).lt.40.) then
              Tsnow(i)=40.
         endif
         f_I(i)=1.
      endif
      if (f_I(i).gt.0.) then	!adjust density
         W_L(i)=W_L(i)+(1-f_I(i))*(snoww(i))
         snowh(i)=snowh(i)-1000/ro_snow(i)*(1-f_I(i))*(snoww(i))
         if (snowh(i).lt.0.) then
              snowh(i)=0.
              snoww(i)=0.
         else
             ro_snow(i)=1000*((snoww(i))/snowh(i))
             snowh(i)=1000/ro_snow(i)*(snoww(i))
         endif
         Hmelt(i)=((1.-f_I(i))*(snoww(i))*1000.*333000.)
         if (Hmelt(i).gt.0.) then
             snowht(i)=snowht(i)-Hmelt(i)      
         endif
         if (W_L(i).gt.0.055*snowh(i)) then
            waterflux(i)=(W_L(i)-0.055*snowh(i))
            snoww(i)=snoww(i)-(W_L(i)-0.055*snowh(i))
            if (snoww(i).lt.0.) then
                 snoww(i)=0.
                 snowh(i)=0.
            else
                ro_snow(i)=1000*((snoww(i))/snowh(i))
                snowh(i)=1000/ro_snow(i)*(snoww(i))
            endif
            snowht(i)=-1000.*333000.*snoww(i)
            Cv(i)=1.9e6*(ro_snow(i))/ro_ice
             if (snowh(i).gt.0.) then
                Tsnow(i)=(snowht(i)+1000.*333000.*snoww(i))/(Cv(i)*snowh(i))+273.15
                W_L(i)=0.055*snowh(i)
             else
                Tsnow(i)=273.15
             endif
         else
            waterflux(i)=0. 
         endif
      else
         waterflux(i)=snoww(i) 
         Tsnow(i)=273.15
         f_I(i)=0.
         snowht(i)=0.
         snowh(i)=0.
         snoww(i)=0.
         W_L(i)=0.
         delta_t=0.
      endif
   else
      f_I(i)=0.
      snowht(i)=0.
      snowht(i)=0.
      snoww(i)=0.
      W_L(i)=0.
      waterflux(i)=0.
      delta_t=0.
   endif
if (waterflux(3).gt.0.) then
     f_I(1)=f_I(1)
endif
enddo
snowheight=snowh(1)+snowh(2)+snowh(3)
snowweight=snoww(1)+snoww(2)+snoww(3)
snowheat=snowht(1)+snowht(2)+snowht(3)
RETURN
END
