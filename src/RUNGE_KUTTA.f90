! ===Ordinary Differential Equation Integrator ====
SUBROUTINE odeint(ystart,nvar,x1,x2,eps,h1,hmin,nok,nbad,derivs,rkqs) 
USE INTEGRATOR
INTEGER nbad,nok,nvar   
REAL eps,h1,hmin,x1,x2,ystart(nvar)
EXTERNAL derivs,rkqs                    ! Derivs IS THE FUNCTION PASSED TO odeint
INTEGER i,kmax,kount,nstp
REAL dxsav,h,hdid,hnext,x,xsav,dydx(NMAX),xp(KMAXX),y(NMAX),yp(NMAX,KMAXX),yscal(NMAX)
COMMON /path/ kmax,kount,dxsav,xp,yp
x=x1
h=sign(h1,x2-x1)    ! CALCULATE STEP SIZE h BY TAKING THE SIGNUM OF h1 AND (x2-x1) ?? ABSOLUTE VALUE OF h1 TIMES THE SIGN OF (x2-x1)
nok=0               ! SUCCESSFUL STEPS: nok
nbad=0              ! BAD STEPS: nbad
kount=0             ! STORED INTERMEDIATE VALUES: kount
do i=1,nvar
	y(i)=ystart(i)
enddo
if (kmax.gt.0) xsav=x-2.*dxsav
do nstp=1,MAXSTP
        call derivs(x,y,dydx)
        do i=1,nvar
          yscal(i)=abs(y(i))+abs(h*dydx(i))+TINY
        enddo
        if(kmax.gt.0)then
          if(abs(x-xsav).gt.abs(dxsav)) then
            if(kount.lt.kmax-1)then
              kount=kount+1
              xp(kount)=x
              do i=1,nvar
                yp(i,kount)=y(i)
              enddo
              xsav=x
            endif
          endif
        endif
        if((x+h-x2)*(x+h-x1).gt.0.) h=x2-x

        call rkqs(y,dydx,nvar,x,h,eps,yscal,hdid,hnext,derivs)
        if(hdid.eq.h)then
          nok=nok+1
        else
          nbad=nbad+1
        endif
        if((x-x2)*(x2-x1).ge.0.)then
          do i=1,nvar
            ystart(i)=y(i)
          enddo
          if(kmax.ne.0)then
            kount=kount+1
            xp(kount)=x
            do i=1,nvar
              yp(i,kount)=y(i)
            enddo
          endif
          return
        endif
        if(abs(hnext).lt.hmin) write(*,*)'stepsize smaller than minimum in odeint'

        h=hnext
enddo
return
END
!------------------------------------------------

! ===== Runge-Kutta Classic 4th Order Method =====

SUBROUTINE rk4(y,dydx,n,x,h,yout,derivs)
USE INTEGRATOR
INTEGER n
REAL h,x,dydx(n),y(n),yout(n)
EXTERNAL derivs
INTEGER i
REAL h6,hh,xh,dym(NMAX),dyt(NMAX),yt(NMAX)
hh=h*0.5
h6=h/6.
xh=x+hh
do i=1,n
   yt(i)=y(i)+hh*dydx(i)
enddo
 call derivs(xh,yt,dyt)
do i=1,n
   yt(i)=y(i)+hh*dyt(i)
enddo
call derivs(xh,yt,dym)
do i=1,n
   yt(i)=y(i)+h*dym(i)
   dym(i)=dyt(i)+dym(i)
enddo
call derivs(x+h,yt,dyt)
do i=1,n
yout(i)=y(i)+h6*(dydx(i)+dyt(i)+2.*dym(i))
enddo
return
END
!------------------------------------------------

! ===== Runge-Kutta Adaptive Size Method =====
!
! USES RKCK SUBROUTINE

SUBROUTINE rkqs(y,dydx,n,x,htry,eps,yscal,hdid,hnext,derivs)
USE INTEGRATOR
INTEGER n
REAL eps,hdid,hnext,htry,x,dydx(n),y(n),yscal(n)
EXTERNAL derivs
! USES derivs,rkck
INTEGER i
REAL errmax,h,htemp,xnew,yerr(NMAX),ytemp(NMAX),SAFETY,PGROW,PSHRNK,ERRCON
PARAMETER (SAFETY=0.9,PGROW=-.2,PSHRNK=-.25,ERRCON=1.89e-4)
h=htry
1 call rkck(y,dydx,n,x,h,ytemp,yerr,derivs)
errmax=0.
do  i=1,n
    errmax=max(errmax,abs(yerr(i)/yscal(i)))
ENDDO
errmax=errmax/eps
if(errmax.gt.1.)then
   htemp=SAFETY*h*(errmax**PSHRNK)
   h=sign(max(abs(htemp),0.1*abs(h)),h)
   xnew=x+h
   if(xnew.eq.x)THEN
		pause 'stepsize underflow in rkqs'
   endif
   goto 1
else
   if(errmax.gt.ERRCON)then
       hnext=SAFETY*h*(errmax**PGROW)
   else
       hnext=5.*h
   endif
   hdid=h
   x=x+h
   do i=1,n
      y(i)=ytemp(i)
   ENDDO
   return
endif
END
!------------------------------------------------

! ===== Runge-Kutta Adaptive Size Method =====
!
! USED IN RKQS ADAPTIVE SIZE METHOD

SUBROUTINE rkck(y,dydx,n,x,h,yout,yerr,derivs)
USE INTEGRATOR
INTEGER n
REAL h,x,dydx(n),y(n),yerr(n),yout(n)
EXTERNAL derivs
!   USES derivs
INTEGER i
REAL ak2(NMAX),ak3(NMAX),ak4(NMAX),ak5(NMAX),ak6(NMAX),&
ytemp(NMAX),A2,A3,A4,A5,A6,B21,B31,B32,B41,B42,B43,B51,B52,B53,&
B54,B61,B62,B63,B64,B65,C1,C3,C4,C6,DC1,DC3,DC4,DC5,DC6
PARAMETER (A2=.2,A3=.3,A4=.6,A5=1.,A6=.875,B21=.2,B31=3./40.,&
B32=9./40.,B41=.3,B42=-.9,B43=1.2,B51=-11./54.,B52=2.5,&
B53=-70./27.,B54=35./27.,B61=1631./55296.,B62=175./512.,&
B63=575./13824.,B64=44275./110592.,B65=253./4096.,C1=37./378.,&
C3=250./621.,C4=125./594.,C6=512./1771.,DC1=C1-2825./27648.,&
DC3=C3-18575./48384.,DC4=C4-13525./55296.,DC5=-277./14336.,&
DC6=C6-.25)
do i=1,n
  ytemp(i)=y(i)+B21*h*dydx(i)
ENDDO
call derivs(x+A2*h,ytemp,ak2)
do i=1,n
   ytemp(i)=y(i)+h*(B31*dydx(i)+B32*ak2(i))
enddo
call derivs(x+A3*h,ytemp,ak3)
do i=1,n
   ytemp(i)=y(i)+h*(B41*dydx(i)+B42*ak2(i)+B43*ak3(i))
enddo
call derivs(x+A4*h,ytemp,ak4)
do i=1,n
  ytemp(i)=y(i)+h*(B51*dydx(i)+B52*ak2(i)+B53*ak3(i)+B54*ak4(i))
enddo
call derivs(x+A5*h,ytemp,ak5)
do i=1,n
   ytemp(i)=y(i)+h*(B61*dydx(i)+B62*ak2(i)+B63*ak3(i)+B64*ak4(i)+B65*ak5(i))
enddo
call derivs(x+A6*h,ytemp,ak6)
do i=1,n
   yout(i)=y(i)+h*(C1*dydx(i)+C3*ak3(i)+C4*ak4(i)+C6*ak6(i))
enddo
do i=1,n
  yerr(i)=h*(DC1*dydx(i)+DC3*ak3(i)+DC4*ak4(i)+DC5*ak5(i)+DC6*ak6(i))
enddo
return
END
