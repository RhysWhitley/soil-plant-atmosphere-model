PROGRAM MAIN_SPA
! Soil Plant Atmosphere model
! Mathew Williams
! University of Edinburgh, EH9 3JU
! mat.williams@ed.ac.uk
! 6 Febuary 2003
USE ERROR
IMPLICIT NONE

INTEGER flag, x, j, i, iter, aci, tenDay, Nrun
REAL iwater
CHARACTER*60    head, input_file, in_fold, meto_h, vege_h, phen_h, soil_h
CHARACTER*100   meto_file, vege_file, phen_file, soil_file

flag=0; x=0; 
input_file = 'file_names.txt'
in_fold = 'inputs/'

! Open text file that contains file names and simulation length
OPEN(22, FILE=input_file, STATUS='old', ACTION='read')
    READ(22,*) head, meto_h
    READ(22,*) head, phen_h
    READ(22,*) head, vege_h
    READ(22,*) head, soil_h
    READ(22,*) head, Nrun
CLOSE(22)

! Create strings by concatenating input directory to file names
meto_file = TRIM(in_fold)//TRIM(meto_h)
phen_file = TRIM(in_fold)//TRIM(phen_h)
vege_file = TRIM(in_fold)//TRIM(vege_h)
soil_file = TRIM(in_fold)//TRIM(soil_h)

! Initialise the model
CALL INIT(iwater, meto_file, phen_file, vege_file, soil_file)	        ! open files - see io.f90

! Run it
DO j=1,Nrun                 ! for each day
	CALL IO_DAILY()			! read daily drivers - see io.f90
	PRINT 1001, j
	1001 FORMAT(' Day: ', I5)
	CALL SOLAR()			! light regime - see light.f90
	CALL BOUNDARY()			! boundary layer and basic soil parameters - see solar air.f90
	CALL DAY()				! carbon and water transfer - see canopy.f90
	CALL DATOUT(iwater)		! output - see io.f90
ENDDO

END PROGRAM MAIN_SPA
!--------------------------------------------------------------**
