# Choose compiler
 compiler =i686-w64-mingw32-gfortran -static-libgcc -static-libgfortran
# compiler = ifort -O0 -g -traceback -fp-stack-check -check bounds
# compiler = gfortran -ffixed-line-length-132

# Declare all object files that are used in SPA and store in
# in the variable objects. There are 11 .f90 files to be compiled.
objects = zbrent.o \
	scale_declarations_48.o \
	all_declarations_jun09.o \
	main.o \
	RUNGE_KUTTA.o \
	soil_functions_Feb03.o \
	soil_air_Sep02.o \
	light_mar03.o \
	canopy_jul09.o \
	io_jun09.o \
	leaf_jul09.o \

# Compile all object files
SPA_win32.exe : $(objects)
	$(compiler) -o SPA_win32.exe $(objects)

# Compile the .f90 files for their respective object files
zbrent.o : zbrent.f90
	$(compiler) -c zbrent.f90
	
scale_declarations_48.o : scale_declarations_48.f90
	$(compiler) -c scale_declarations_48.f90

all_declarations_jun09.o : all_declarations_jun09.f90
	$(compiler) -c all_declarations_jun09.f90

main.o : main.f90
	$(compiler) -c main.f90

RUNGE_KUTTA.o : RUNGE_KUTTA.f90
	$(compiler) -c RUNGE_KUTTA.f90

soil_functions_Feb03.o : soil_functions_Feb03.f90
	$(compiler) -c soil_functions_Feb03.f90

soil_air_Sep02.o : soil_air_Sep02.f90
	$(compiler) -c soil_air_Sep02.f90

light_mar03.o : light_mar03.f90
	$(compiler) -c light_mar03.f90

canopy_jul09.o : canopy_jul09.f90
	$(compiler) -c canopy_jul09.f90

io_jun09.o : io_jun09.f90
	$(compiler) -c io_jun09.f90

leaf_jul09.o : leaf_jul09.f90
	$(compiler) -c leaf_jul09.f90

# Clean makefile
clean :
	rm SPA_win32.exe $(objects)
