SUBROUTINE DAY()
! CANOPY models (previously called day_sunshade_soilwater)
! Runs first by timestep, and within each step then runs through each layer
! soilwater version 18 feb 1999
! {calculate daily carbon gain for one layer}
! New parallel calculations of deltaLWP for sun and shade fractions (29 June 1998).
USE CLIM
USE VEG
USE METEO
USE METAB
USE IRRADIANCE_SUNSHADE
USE ERROR
USE HOURSCALE
USE SOIL_STRUCTURE
IMPLICIT NONE  

INTEGER t, i, k
REAL res, totestevap
REAL modet, agr, hold_psil, sun_psil, shade_psil, gsm, weighted_gs(numl)
REAL la_shade, la_sun, frac_sun, frac_sh, numsecs, lambda
! Temporary parameters out of ASSIMILATE (Rhys Jul 09)
REAL :: Ci_Sun, Ci_Shade, ci_av, lt_Sun, lt_Shade, lt_av, LAYTIME,	&
		gs_Sun, gs_Shade, A_Sun, A_Shade, APAR


numsecs = 3600.*24./steps	! No. of seconds per timestep
errday = daynum			    ! Set day into error checker
thawed = 1

! === MAKE CALCULATIONS FOR ALL DAYS [ t in 1:NUMBER_OF_STEPS ] === 
DO t=1,steps
    CALL SOILDAY(t,totestevap)	! See soil functions.f90
    psis = weighted_SWP			! Weighted soil water potential
	weighted_gs = 0.			! Weighted stomatal conductance for each layer, based on sun and shaded fractions - a diagnostic variable
  
  ! === MAKE CALCULATIONS FOR ALL LAYERS [ i in 1:NUMBER_OF_LAYERS ] === 
    DO i=1,numl           
	    psil = LWPstore(i)	            ! Load layer's LWP from previous timestep (30 mins ago) FOR CANOPY LAYER (i)
        co2amb = coa(t)		            ! Load current CO2 concentration                FOR DAY (t)
	    atmos_pres = pressure(t)        ! *** LOAD ATMOSPHERIC PRESSURE                 FOR DAY (t)
	    hold_psil = psil	            ! Store LWP 
	    frac_sun = leaf_sun(t,i)        ! *** LOAD SUNLIT FRACTION OF LEAF              FOR DAY (t) IN CANOPY LAYER (i)
	    la_sun = frac_sun*lai(i)	    ! First do sunlit leaf area                     FOR CANOPY LAYER (i)
	    la = la_sun                     ! *** SET SUNLIT LAI *** <RHYS>
	    IF((la.gt.0.).and.(totestevap.gt.0.)) THEN	        ! If there is active leaf area
		    nit = frac_sun*Nla(i)
		    par = (parabs_sun(t,i) + parabs_shade(t,i)*frac_sun)/la	                        ! Sunlit leaf area has all beam rad plus frac of diffuse rad
		    rad = 0.001*(nirabs(t,i)*frac_sun/la + longem(t,i)*frac_sun/la + par/par_ratio)	! Net radiation = shortwave + longwave radiation
		    temp = temptop(t) - 0.1111*(i-1)*(temptop(t)-tempbot(t))                        ! TEMPERATURE
		    wdef = wdtop(t) - 0.1111*(i-1)*(wdtop(t)-wdbot(t))                              ! ABSOLUTE WATER DEFICIT
		    gbb = gbw(i,t)		                            ! Boundary layer
		    CALL SET_LEAF( i, frac_sun )	            ! ** changed so a C4 Vcmax is read in (May 09)
	        CALL ASSIMILATE(modet,agr,t,i,res,coa(t),gsm,Ci_Sun,lt_Sun)	! SEE leaf.f90
		    gs_Sun = gsm									! Sunlit gs
			A_Sun = agr										! Sunlit A
			gppt(t) = gppt(t)+agr		                    ! umol CO2 assimilation m-2 ground area s-1
		    respt(t) = respt(t)+res		                    ! umol CO2 respiration m-2 ground area s-1
		    transt(t) = transt(t)+modet	                    ! Evapotranspiration in W m-2 ground area
		    checkpar(t,i) = checkpar(t,i)+par*la
		    weighted_gs(i) = gsm*la/lai(i)
	    ENDIF
	    sun_psil = psil	                ! Record LWP in sun leaves
	
	    ! === REPEAT THE PROCESS FOR SHADED LEAVES ===  <RHYS>
	    psil = hold_psil	            ! Reset LWP for shade leaf calculation
	    frac_sh = (1.-leaf_sun(t,i))    ! *** SHADED LEAF FRACTION ***  <RHYS>
	    la_shade = frac_sh*lai(i)       ! *** CALCULATE SHADED LEAF AREA ***  <RHYS>
	    la = la_shade                   ! *** SET SHADED LAI ***  <RHYS>
	    IF((la.gt.0.).and.(totestevap.gt.0.)) THEN
	        nit = frac_sh*Nla(i)
            par = parabs_shade(t,i)*frac_sh/la
	        rad = 0.001*(nirabs(t,i)*frac_sh/la + longem(t,i)*frac_sh/la + par/par_ratio)	! Net radiation= shortwave + longwave radiation
	        temp = temptop(t) - 0.1111*(i-1)*(temptop(t)-tempbot(t))
            wdef = wdtop(t) - 0.1111*(i-1)*(wdtop(t)-wdbot(t))
            
            ! === BOUNDARY LAYER ===  <RHYS>
            gbb = gbw(i,t)
	        CALL SET_LEAF(i,frac_sh)					    ! SEE below
            CALL ASSIMILATE(modet,agr,t,i,res,coa(t),gsm,Ci_Shade,lt_Shade)	! SEE leaf.f90
			gs_Shade = gsm
			A_Shade = agr	
			gppt(t) = gppt(t)+agr		                    ! umol CO2 assimilation (m-2 ground area s-1)
	        respt(t) = respt(t)+res		                    ! umol CO2 respiration (m-2 ground area s-1)
	        transt(t) = transt(t)+modet		                ! Evapotranspiration in (W m-2 ground area)
	        checkpar(t,i) = checkpar(t,i)+par*la
	        weighted_gs(i) = weighted_gs(i)+gsm*la/lai(i)
        ENDIF
	    shade_psil = psil	                        ! Record LWP in shade leaves
	    psil = frac_sun*sun_psil+frac_sh*shade_psil	! Calculate final LWP
	    LWPstore(i) = psil
	! Need to do this so that Ci and Tl don't go to zero - result of the LA check above
	    IF(la_sun == 0) THEN
			ci_sun = ci_shade
			lt_sun = lt_shade
		ENDIF
		Ci_av = (Ci_Sun+Ci_Shade)/2.0
		lt_av = (lt_Sun+lt_Shade)/2.0
		
	! Export absorbed PAR to calculate LUE	
		APAR = parabs_sun(t,i) + parabs_shade(t,i)
		
! Write canopy layer outputs - Rhys improvement (July 09)
    IF(outt == 1.0)THEN
	! Determine time-step
		LAYTIME = REAL(daynum)+REAL(t-1)/REAL(steps)
	! Write ooutput file
		WRITE(i+40,'(	F10.4,",",F8.2,",",F8.2,",",F8.2,",",F8.3,",",F8.3,",",F8.3,",",F5.2,",",F6.1,",",F6.1,		&
						& ",",F6.2,",",F8.2,",",F10.2,",",F10.2,",",F10.2,",",F10.2,",",F10.2,",",F10.2,",",F10.2,	&
						& ",",F10.2)')																				&
						& LAYTIME, weighted_gs(i), gs_Sun, gs_Shade, gppt(t), A_Sun, A_Shade, respt(t), psil, Ci_av,&	
						& co2amb, transt(t), wdef, APAR, rad, lt_av, temp, gbb, lai(i)
	ENDIF    
    
    ENDDO
    
    ! ==== TOTAL EVAPOTRANSPIRATION DURING THIS TIME PERIOD ====
    lambda = 1000.*(2501.0-2.364*temp)	    ! Latent heat of vapourisation, (J kg-1)
    totet = 0.001*numsecs*transt(t)/lambda	! (m3 m-2 t-1, m t-1)
    totevap = totevap+1000.*totet	! (mm)
        
ENDDO

RETURN
END

!--------------------------------------------------------------------
SUBROUTINE SET_LEAF(clayer,frac)		! Sets Farquhar parameters and hydraulic parameters for each leaf layer

USE METAB
USE VEG
USE METEO
IMPLICIT NONE

INTEGER clayer
REAL frac

c3path = c3(clayer)

! Metabolic rates are umol/m2/s - so we assume here that nit is N/m2 (it's actually N/clayer)
PATHWAY: SELECT CASE(c3path)
	CASE(1)
		vcm = C3KAP_VC*nit/la	! Metabolic constant for C3 Vcmax
		vjm = C3KAP_VJ*nit/la	! Metabolic constant for C3 Jmax	
	CASE(2,3)
		vcm = C4KAP_VC*nit/la	! Metabolic constant for C4 Vcmax
		vpm = C4KAP_VP*nit/la	! Metabolic constant for C4 Vpmax
		vjm = C4KAP_VJ*nit/la	! Metabolic constant for C4 Jmax
END SELECT PATHWAY

rn = 0.105*propresp   ! {respiration constant umol CO2/g N at 10 deg C}
rn = rn/la	! Convert to resp per m2

ht = layerht(clayer)
! Plant hydraulic resistances are determined by the amount of leaf area in the sunlit or shaded fraction
IF(conductivity.eq.1) THEN
	rplant = ht/(gplant*la)	    ! MPa s mmol-1 (per layer)
ELSE
	rplant = 1./(gplant*la)		! conductance is constant with height
ENDIF
layer_capac = capac*la

rsoil = canopy_soil_resistance(clayer)	! soil + root resistance of each canopy layer

! Now correct rsoil according to the fraction of total leaf area in this run
rsoil = rsoil/frac

RETURN
END
