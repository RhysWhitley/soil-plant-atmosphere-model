SUBROUTINE SOLAR()  
! Correction mar 03 to sun and shade routines
! version 4 allows input of diffuse fraction data
! corrected 9-10 April 1999 (sun and shade component of ATTENUATE_PAR)
! modified SOLAR, to partition absorption between 
! sunlit and shaded fractions in each layer
! new FRACDIF - hourly variation in diffuse fraction
! version 4, 3 Dec 1997
! NIRABS is now truly just near-infra-red, not total short-wave as in previous versions
! correction 15 Oct 1998 - set intbm and intdf =0 at start of attentuate_long and attenute_nir routines (par was correct already)
USE CLIM
USE VEG
USE IRRADIANCE_SUNSHADE
USE HOURSCALE
IMPLICIT NONE

INTEGER i, t, count, aci
REAL pi, dayd, fdiff, xhour, sun, dec, totlong, clump
REAL kbm, upnir(90:101), downnir(90:101), uppar(90:101), downpar(90:101)
REAL uplong(90:101), downlong(90:101), sump, sumn, suml, ff, check
REAL beam, diff, nirbeam, nirdiff, totnir, ab, long, estfdiff, soilt
REAL absnir(numl), abspar_sun(numl), skyabsp, skyabsn, soilabsp, soilabsn, lnet
REAL skyabsl, soilabsl, abslong(numl), em, soem, totpar, boltz, temp(10), radnet
REAL period, ttt, fracdif, abspar_shade(numl), sunfrac(numl), totparabs(steps,numl)
REAL ttimt, SWR, PId, PIb, fracdifhour, estlong, skytemp, shifter      

period = REAL(steps)
clump = 1. ! Clumping factor - =1 means unclumped, random distribution
ff = 1.0   ! Correction factor for stems etc on attenuation, currently unused and untested, set at 1.0.
boltz = 5.6697e-8
pi = 3.1415
dayd = REAL(daynum)
totparabs = 0.
! Solar characteristics
dec = -23.4*COS((360.*(dayd+10.)/365.)*pi/180.)*pi/180.
! Diffuse fraction of radiation (Amthor,1994)
estfdiff = FRACDIF(dec,lat,partop)	! Estimate diffuse fraction of radiation from measured vs maximum radiation
! Day is divided into 48 1/2hour-long periods
ttt = 0.0
fdiff = estfdiff

DO t=1,steps		! Run through each timestep
	ttt = ttt+1. 
	IF(mfdiff(t).gt.-90.) THEN  ! If diffuse fraction data is available use it
		fdiff = mfdiff(t)
	ELSE	! Use estimate
		fdiff = estfdiff
	ENDIF
	! Reset diffuse arrays
	DO i=90,101
		downnir(i) = 0.0
		upnir(i) = 0.0
		downpar(i) = 0.0
		uppar(i) = 0.0
		downlong(i) = 0.0
		uplong(i) = 0.0
	ENDDO
	DO i=1,10
		absnir(i) = 0.0
		abspar_sun(i) = 0.0
		abspar_shade(i) = 0.0
		abslong(i) = 0.0
		temp(i) = temptop(t)-0.1111*(i-1)*(temptop(t)-tempbot(t))
	ENDDO
	
	! Detemine light extinction coefficent from sun angle
	xhour = pi/180.*15.*(ttt-0.5*period)*24./period	! Use period = REAL(steps)
	! SIN(beta) - solar geometry affects attenuation
	sun = COS(lat)*COS(xhour)*COS(dec)+SIN(lat)*SIN(dec)
    
	! Determine extinction coefficient for direct beam radiation
	IF(sun.lt.0.06) THEN	! At low sun angles extinction coefficient is zero (only occurs near sunrise and sunset - without this correction we get v unREAListic estimates)
		sun = 0.
		kbm = 0.0
	ELSE
		kbm = 1./(spherical*sun)
	ENDIF
	
	! If sun has risen, then partition par between beam and diffuse
	IF(sun.gt.0.) THEN	
		beam = (1.-fdiff)*partop(t)	! Light attentuation(PPFD) initialise layer above canopy
		diff = fdiff*partop(t)
	ELSE 
		beam = 0.
		diff = partop(t)
	ENDIF
	
	SWR = swrad(t)				! You can estimate short wave radiation from PAR data - 4.6 umol J-1, 50% of SWR is PAR	new 19/Mar/97, i.e.  =partop(t)/(0.5*par_ratio)	
	PIb = 0.48*(1.-fdiff)*SWR
	PId = 0.5*SWR-PIb
	nirbeam = (1.-fdiff)*SWR-PIb
	nirdiff = fdiff*SWR-PId
	long = 0.001*boltz*(temptop(t)+273.15-20.)**4	! Determine longwave incident from sky (Jones p.27) kW m-2 long=.350
	totnir = nirbeam+nirdiff
	totpar = beam+diff
	totlong = long
	
	! Set values to zero
	soilabsn=0.; soilabsp=0.; skyabsp=0.; skyabsn=0.
	soilabsl=0.; skyabsl=0.; count=0; em=0.; ab=0.
	
	! Start the attenuation routines
	DO WHILE(count.lt.3)			! Multiple passes through the canopy - 3 is enough to account for every photon in general
		IF(totpar.gt.1.) THEN		! If the sun is up, then call the routines....
			CALL ATTENUATE_PAR(ff,lai,beam,diff,parrefl,partrans,sump,&		! Firsly calculate PAR attenuation - only do sunlit and shaded calculations for PAR
									soilpar,abspar_sun,abspar_shade,uppar,downpar,soilabsp,skyabsp,kbm,sunfrac,clump)
			IF(count.eq.0.) THEN	! Only set leaf_sun after first pass, when beam radiation is incident
				DO i=1,10
					leaf_sun(t,i) = sunfrac(i)
				ENDDO
			ENDIF
			CALL ATTENUATE_NIR(ff,lai,nirbeam,nirdiff,nirrefl,nirtrans,sumn,&	! Next do Near Infra Red
								soilnir,absnir,upnir,downnir,soilabsn,skyabsn,kbm,clump)
		ELSE
			abspar_sun = 0.0; abspar_shade = 0.0
			absnir = 0.0; sunfrac = 0.0
		ENDIF
		!soilt = hourts-freeze	! Use surface temp from previous timestep
		soilt = temptop(t)	! Use air temp from current timestep - 
		CALL LONGWAVE(ff,lai,long,suml,abslong,uplong,downlong,&		! Longwave
							soilabsl,skyabsl,count,temp,soilt,em,soem,ab,clump)
		beam=0.; diff=0.; nirbeam=0.; nirdiff=0.
		long=0.; radnet=0.; lnet=0.
		count = count+1 
		!WRITE(41,'(I4,I4,14(",",F7.2))')t,count,(abspar(11-i),i=1,10),sum(abspar),totpar
		!WRITE(41,'(14(",",F7.2))')sum(lai),sum(abspar),totpar
	ENDDO
	

	! ====== Calculate output for use in other routines ====== !
	DO i=1,10
		parabs_sun(t,i) = abspar_sun(11-i)			! Beam par absorbed in each layer
		parabs_shade(t,i) = abspar_shade(11-i)		! Diffuse par absorbed in each layer
		! Total shortwave radiation for heating = par + nir , per m2 ground area per layer
		nirabs(t,i) = absnir(11-i)
		longem(t,i) = abslong(11-i)*1000.
		lnet = lnet + longem(t,i)
		radnet = radnet + nirabs(t,i) 
		totparabs(t,i) = abspar_sun(11-i) + abspar_shade(11-i)
	ENDDO
	
	soilnet(t) = soilabsn+1000.*soilabsl + soilabsp/par_ratio	! Net radiation= shortwave + longwave radiation
	check = SUM(abspar_sun)+SUM(abspar_shade)+soilabsp+skyabsp
	ttimt = REAL(daynum)+REAL(t)/steps
    WRITE(52,'(6(f10.6,","))') ttimt, totlong, skyabsl, lnet, radnet, SUM(totparabs(t,:))
	
	IF(partop(t).gt.0) THEN
		WRITE(51,'(24(F7.2,","))')ttimt,soilnet(t),soilabsn,soilabsl,soilabsp,partop(t)	! Net radiation = shortwave + longwave radiation
		WRITE(51,'(24(F7.2,","))')ttimt,SUM(abspar_sun),(1.-fdiff)*partop(t),(parabs_sun(t,i),i=1,10)
		WRITE(51,'(24(F7.2,","))')ttimt,SUM(abspar_shade),fdiff*partop(t),(parabs_shade(t,i),i=1,10),skyabsp
		WRITE(51,'(24(F7.2,","))')ttimt,fdiff,(leaf_sun(t,i),i=1,10),check
	ENDIF
	modRnet(t) = SWR-skyabsn-skyabsp/par_ratio-1e3*(skyabsl-totlong)


ENDDO

checkpar = 0.

RETURN
END
!-----------------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------------
REAL FUNCTION FRACDIF(dec,lat,par)
! Determines actual:potential radiation ratio (ksko) for the day, from total potential daily PAR,
! and then uses a relationship from Erbs et al (1982) to estimate
! Fraction of incoming radiation that is diffuse
USE CLIM
IMPLICIT NONE

INTEGER t
REAL dec, lat, par(steps), ff, mult, dayint
REAL parday, pi, dayl, So, ks, ko, ksko, secs

secs = 24./steps*3600.		! # of seconds per timestep
So = 1360.					! Solar constant (Wm-2)
pi = 3.14159

ks = DAYINT(partop,0.)*1e-6/2.3		! TOTAL PAR FOR THE DAY (MJ m-2 d-1)
mult = TAN(lat)*TAN(dec)

IF(mult.ge.1.) THEN
	dayl = 24.0
ELSE IF(mult.le.-1.) THEN
	dayl = 0.0
ELSE
	dayl = 24.*ACOS(-mult)/pi
ENDIF

! Estimate total daily extraterrestrial PAR
! Total daily extraterrestrial radiation (MJ m-2)
ko = 0.0864*So*(dayl/24.*SIN(dec)*SIN(lat)+1./PI*COS(lat)*COS(dec)*SIN(dayl*PI/24.))
! Daily ksko
ksko = ks/ko

! Diffuse fraction
IF(ksko.lt.0.22) THEN
	ff = 1.0-0.09*ksko
ELSE IF(ksko.lt.0.8) THEN
	ff = 0.9511 - 0.1604*ksko + 4.388*ksko**2 - 16.638*ksko**3 + 12.336*ksko**4
ELSE 
	ff = 0.165
ENDIF
fracdif = ff 

!WRITE(62,*)daynum,ks,ko,ff
RETURN

END
!-------------------------------------------------------------------
REAL FUNCTION FRACDIFhour(dec,lat,parsteps,time)
! Determines actual:potential radiation ratio (ksko) for the day, from total potential daily PAR,
! and then uses a relationship from Erbs et al (1982) to estimate
! fraction of incoming radiation that is diffuse
USE CLIM
IMPLICIT NONE
REAL dec, lat, ff, time, period, hourangle
REAL parsteps, pi, So, ks, ko, ksko, secs

period = REAL(steps)
secs = 24./period*3600.	! # 0f seconds per timestep
So = 1360.		! Solar constant (Wm-2)
pi = 3.14159
ks = parsteps/2.3		! MJ m-2 d-1
hourangle = pi/180.*15.*(time-0.5*period)*24./period	! Use period = REAL(steps)
ko = So*(SIN(dec)*SIN(lat)+COS(lat)*COS(dec)*COS(hourangle)) ! Extraterrestrial radiation (MJ m-2)
ksko = ks/ko  ! hourly ksko
! *Diffuse fraction
IF(ksko.lt.0.22)THEN
   ff = 1.0-0.09*ksko
ELSE IF(ksko.lt.0.8)THEN
   ff = 0.9511-0.1604*ksko + 4.388*ksko**2 - 16.638*ksko**3 + 12.336*ksko**4
ELSE 
   ff = 0.165
ENDIF
fracdifhour = ff 
!WRITE(35,*)ks,ko,ff
RETURN
END
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
SUBROUTINE LONGWAVE(ff,lai,df,sum,absorb,updf,downdf,soilrad,skyrad,count,Ta,Ts,&
totemit,soilem,totab,clump)

USE CANOPY
IMPLICIT NONE

INTEGER i, count
REAL ff, lai(numl), intdf, eup, edown, totab
REAL clump, kdf, updf(90:101)
REAL downdf(90:101), df
REAL soilrad, skyrad, Ta(numl)
REAL absorb(10), sum, leafrad, decay
REAL boltz, emiss, Ts, totemit, soilem

boltz = 5.6799e-8; intdf = 0.0
emiss = 0.96
downdf(101) = df
! Diffuse radiation approximated by beta=30 degrees
kdf = 0.5

DO i=100,91,-1
	decay = EXP(-kdf*clump*lai(101-i))
	IF(count.eq.0) THEN ! Longwave radiative heat loss from top side of leaf (KW m-2)
		eup = 0.001*emiss*boltz*(Ta(101-i)+273.15)**4*(1.-decay)		!'*(1-decay)' Corrects emissions into the 1-D, vertical
		totemit = totemit+2.*eup*lai(101-i)
	ELSE
		eup=0.
	ENDIF
	! And bottom side
	edown = eup
	downdf(i) = downdf(i)+downdf(i+1)*decay+edown
	intdf = downdf(i+1)*(1.-decay)
	! Correct for transmittance (trans) and reflectance (refl) & PAI/LAI ratio
	absorb(i-90) = absorb(i-90)+intdf*emiss/ff-eup-edown
	! Add transmitted diffuse to downward diffuse
	downdf(i) = downdf(i)+0.5*(1.-emiss)*intdf
	! Add reflected diffuse to upward diffuse
	updf(i) = updf(i)+0.5*(1.-emiss)*intdf+eup
	totab = totab+intdf*emiss
ENDDO

! Reflectance by soil of radiation that penetrates all vegetation
soilem = emiss*boltz*(Ts+273.15)**4*0.001

IF(count.eq.0) THEN
	updf(90) = 0.04*downdf(91)+soilem
ELSE
	updf(90) = 0.04*downdf(91)
ENDIF

soilrad = soilrad+0.96*downdf(91)

DO i=91,101
	downdf(i) = 0.0
ENDDO

DO i=91,100
	! Now return upwards through the canopy, dealing with refelcted radiation
	! unintercepted
	decay = EXP(-kdf*clump*lai(101-i))	!corrected 23 Feb 1997
	updf(i) = updf(i)+updf(i-1)*decay
	! Intercepted
	intdf = updf(i-1)*(1.-decay)
	! Absorbed determined from transmittance/reflectance
	absorb(i-90) = absorb(i-90)+intdf*emiss/ff
	! Add reflected beam & diffuse to downward diffuse
	downdf(i) = downdf(i)+0.02*intdf
	! Add transmitted beam & diffuse to upward diffuse
	updf(i) = updf(i)+0.02*intdf
	updf(i-1) = 0.0
	totab = totab+intdf*0.96
ENDDO

skyrad = skyrad+updf(100)
updf(100) = 0.0
leafrad = 0.0

DO i=1,10
	leafrad = leafrad+absorb(i)
ENDDO

sum = soilrad+skyrad+leafrad
      
RETURN 
END
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
SUBROUTINE ATTENUATE_PAR(ff,lai,bm,df,refl,trans,sum,reflsoil,absorb_sun,absorb_shade,updf,downdf,soilrad,skyrad,kbm,sunfrac,clump)

USE CANOPY
IMPLICIT NONE

INTEGER i
REAL ff, lai(numl), trans, refl, intdf, intbm
REAL clump, kbm, kdf, beam(90:101), updf(90:101)
REAL downdf(90:101), bm, df, suncum, sunprev, cumlai
REAL reflsoil, soilrad, skyrad, sunla(numl), sunfrac(numl)
REAL absorb_sun(numl), sum, leafrad, decay, absorb_shade(numl)

beam=0.0; intbm=0.0; intdf=0.0; sunla=0.0; sunfrac=0.0
beam(101) = bm
downdf(101) = df
cumlai=0.0; sunprev=0.0
! Diffuse radiation approximated by beta=30 degrees
kdf = 0.5

DO i=100,91,-1	! Determine unintercepted radiation
	IF((kbm.gt.0.).and.(bm.gt.0.)) THEN ! Determine sun-lit leaf area 
		cumlai = cumlai+lai(101-i) ! First calculate cumulative leaf area
		suncum = (1-EXP(-kbm*cumlai))/kbm		! Then total sunlit for cumlai. kbm = 1/(2SINBeta)
		sunla(101-i) = suncum-sunprev		! Sunlit area in this layer is cumulative sunlit minus previous
		sunprev = suncum					! Save previous
		IF(sunla(101-i).gt.0.) THEN		! Determine sunlight fraction
			sunfrac(101-i) = sunla(101-i)/lai(101-i)
		ELSE
			sunfrac(101-i) = 0.0
		ENDIF
		beam(i) = bm	! Beam radiation on sunlit leaves is constant 
		intbm = bm*kbm*sunla(101-i)	! Intercepted radiation dI= Io.k.Ls  (dI=intercepted rad, Io=downwelling beam, Ls =sunlit leaf area, k=extinction
   	ELSE
		intbm = 0.0
	ENDIF
	
	! Now for diffuse
	decay = EXP(-kdf*clump*lai(101-i))			! Attenuation factor
	downdf(i) = downdf(i)+downdf(i+1)*decay		! Diffuse radiation that passes through layer
	intdf = downdf(i+1)*(1.-decay)				! Intercepted diffuse radiation in each layer
	downdf(i)=downdf(i)+trans*(intbm+intdf)		! Add transmitted beam & diffuse to downward diffuse
	updf(i)=updf(i)+refl*(intbm+intdf)			! Add reflected beam & diffuse to upward diffuse
	absorb_sun(i-90) = absorb_sun(i-90) + intbm*(1.-trans-refl)/ff	! Absorption of direct beam (correct interception for transmittance and reflectance)
	absorb_shade(i-90) = absorb_shade(i-90) + intdf*(1.-trans-refl)/ff	! Absorption of diffuse
ENDDO

IF(bm.gt.0.) THEN
	beam(91) = (1./kbm-suncum)*bm*kbm		! Direct beam radiation reaching soil surface is directly calculated by this eqn.
ENDIF

! Reflectance by soil of radiation that penetrates all vegetation
updf(90) = reflsoil*(beam(91)+downdf(91))
soilrad = soilrad+(1.-reflsoil)*(beam(91)+downdf(91))
!WRITE(31,'(I4,14(",",F7.2))')t,(beam(i)+downdf(i),
!  1      i=100,91,-1),bm,df
DO i=91,101
	downdf(i) = 0.0
ENDDO

DO i=91,100
	! Now return upwards through the canopy, dealing with reflected radiation
	! Unintercepted
	decay = EXP(-kdf*clump*lai(101-i))
	
	updf(i) = updf(i)+updf(i-1)*decay
	! Intercepted
	intdf = updf(i-1)*(1.-decay)
	! Absorbed determined from transmittance/reflectance
	! absorb_sun(i-90) = absorb_sun(i-90)+sunfrac(101-i)*intdf*(1.-trans-refl)/ff
	! absorb_shade(i-90) = absorb_shade(i-90)+(1.-sunfrac(101-i))*intdf*(1.-trans-refl)/ff
	absorb_shade(i-90) = absorb_shade(i-90) + intdf*(1.-trans-refl)/ff
	! Add reflected beam & diffuse to downward diffuse
	downdf(i) = downdf(i)+refl*intdf
	! Add transmitted beam & diffuse to upward diffuse
	updf(i) = updf(i)+trans*intdf
	updf(i-1) = 0.0
	
ENDDO

skyrad = skyrad+updf(100)
updf(100) = 0.0
leafrad = 0.0

DO i=1,10
	leafrad = leafrad+absorb_sun(i)+absorb_shade(i)
ENDDO

sum = soilrad+skyrad+leafrad

RETURN 
END
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
SUBROUTINE ATTENUATE_NIR(ff,lai,bm,df,refl,trans,sum,reflsoil,absorb,updf,downdf,soilrad,skyrad,kbm,clump)

USE CANOPY
IMPLICIT NONE

INTEGER i
REAL ff, lai(numl)
REAL trans, refl, intdf, intbm
REAL clump
REAL kbm, kdf, beam(90:101), updf(90:101)
REAL downdf(90:101), bm, df
REAL reflsoil, soilrad, skyrad
REAL absorb(10), sum, leafrad, decay

intbm = 0.0; intdf = 0.0
beam = 0.0
beam(101) = bm
downdf(101) = df
! Diffuse radiation approximated by beta=30 degrees
kdf = 0.5

DO i=100,91,-1
	! Determine unintercepted radiation
	beam(i) = beam(i+1)*EXP(-kbm*clump*lai(101-i))
	! And thus intercepted radiation
	intbm = beam(i+1)-beam(i)
	! Now for diffuse
	decay = EXP(-kdf*clump*lai(101-i))
	downdf(i) = downdf(i)+downdf(i+1)*decay
	intdf = downdf(i+1)*(1.-decay)
	! Correct for transmittance (trans) and reflectance (refl)
	absorb(i-90) = absorb(i-90) + intbm*(1.-trans-refl)/ff
	absorb(i-90) = absorb(i-90) + intdf*(1.-trans-refl)/ff
	! Add transmitted beam & diffuse to downward diffuse
	downdf(i) = downdf(i)+trans*(intbm+intdf)
	! Add reflected beam & diffuse to upward diffuse
	updf(i) = updf(i)+refl*(intbm+intdf)
ENDDO

! Reflectance by soil of radiation that penetrates all vegetation
updf(90) = reflsoil*(beam(91)+downdf(91))
soilrad = soilrad+(1.-reflsoil)*(beam(91)+downdf(91))
!     WRITE(31,'(I4,14(",",F7.2))')t,(beam(i)+downdf(i),
!    1      i=100,91,-1),bm,df

DO i=91,101
	downdf(i) = 0.0
ENDDO

DO i=91,100
	! Now return upwards through the canopy, dealing with refelcted radiation
	! Unintercepted
	decay = EXP(-kdf*clump*lai(101-i))
	updf(i) = updf(i)+updf(i-1)*decay
	! Intercepted
	intdf = updf(i-1)*(1.-decay)
	! Absorbed determined from transmittance/reflectance
	absorb(i-90) = absorb(i-90) + intdf*(1.-trans-refl)/ff
	! Add reflected beam & diffuse to downward diffuse
	downdf(i) = downdf(i) + refl*intdf
	! Add transmitted beam & diffuse to upward diffuse
	updf(i) = updf(i) + trans*intdf
	updf(i-1) = 0.0
ENDDO

skyrad = skyrad+updf(100)
updf(100) = 0.0
leafrad = 0.0

DO i=1,10
	leafrad = leafrad+absorb(i)
ENDDO

sum = soilrad+skyrad+leafrad

RETURN 
END
