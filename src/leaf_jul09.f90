! LEAF MODELS (previously known as 'newfunctions')
!July 09 contains C4 photosynthesis pathway
SUBROUTINE ASSIMILATE(etr,agr,time,clayer,res,cca,gsm,ci_out,lt_out)
!  Assimilation is determined through equalising assimilation
! according to the Farquhar model and a diffusion model, through
! varying internal CO2 concentration (Ci).
! Rsoil calculations have been moved to manaus_soilair
! switch variables added to specify 
!	-conductance vs conductivity
!	-Farquhar parameters are N-linked or not
! Rhys (July 09) - I have added Ci, T.leaf and Psi.leaf to be extra outputs
USE VEG
USE METEO
USE METAB
USE ERROR
USE C13
implicit none
integer time,clayer,outf,storeday
real g1,g2,etr,lt,leaftemp,asn,gs,ad,agr,res,xacc,cca,maxg,check1,check2
real evap,lambda,dpsildt,boltz,netrad,darkresp,darket,secs,prevpsil,conv,gsm
REAL :: lt_out, ci_out
! function declarations
real ZBRENT,STOMDIFF,COMP
EXTERNAL STOMDIFF
errtime=time;errlayer=clayer
et=0.; asn=0.; ci=0.; an=0.;	ad=0.	! reset
secs=24./steps*3600.	! # 0f seconds per timestep
xacc=0.0001
boltz=5.6703e-11

gs=0.00001

c3path=c3(clayer)	!determine PS pathway for layer

! Brent's method to determine gs (m s-1)
g1=0.00005
g2=0.05

! check if conditions allow photosynthesis
check1=stomdiff(g1)
check2=stomdiff(g2)
maxg=0.002; lcp=0.1

IF((lcp.gt.0.05).and.(check1*check2.lt.0)) THEN 
	IF(maxg.gt.0.00002)THEN 
		gs=zbrent(stomdiff,g1,g2,xacc)
	ELSE		!drought limitation - psil has fallen to minpsil (=minlwp)
		gs=maxg
	ENDIF
!	lt=leaftemp(gs)
	prevpsil=psil
	CALL DELTAPOTENTIAL(psil)	!change in leaf water potential
	dpsildt=psil-prevpsil
	lt=leaftemp(gs)
    netrad=rad-4.*0.96*boltz*(temp+273.15)**3*(lt-temp)
    et=evap(gs,lt,netrad,wdef,gbb)*1.0/18.0
	res=resp*la			! determine total leaf respiration
	agr=(an+resp)*la	! convert from m-2 to clayer total gross rates (resp is related to [N] not LAI)
ELSE	! dark
    gs=0.00004
    ci=co2amb
    lt=leaftemp(gs)
    netrad=rad-4.*0.96*boltz*(temp+273.15)**3*(lt-temp)
    et=evap(gs,lt,netrad,wdef,gbb)*1.0/18.0		!convert from g m-2 s-1 to mol m-2 s-1
    darkresp=rn*nit*exp(log(2.0)*(lt-10.0)/10.0)
    an=-darkresp
    res=darkresp*la	! convert from m-2 to clayer total for net rates
    agr=0.0
	prevpsil=psil
	CALL DELTAPOTENTIAL(psil)	!change in leaf water potential
	dpsildt=psil-prevpsil
ENDIF
lambda=0.001*(2501.0-2.364*temp)	! kJ g-1
etr=et*18.*1000.*lambda*la			! convert et (mol m-2 s-1) to latent heat (Wm-2)
flux(time,clayer)=flux(time,clayer)+la*et*1000.+layer_capac*dpsildt/secs	!water flux at base of trunk
conv=1000.*atmos_pres/((lt+273.15)*Rcon)	!convert from m s-1 to mmol m-2 s-1
gsm=gs*conv

ci_out = ci	! Pass back to CANOPY
lt_out = lt	! Pass back to CANOPY

IF(agr.gt.0.)THEN	!if photosynthesis is occurring
	sumaci(clayer)=sumaci(clayer)+an*la/lai(clayer)*ci	!correct for fraction of total lai in sun or shaded part
	suma(clayer)=suma(clayer)+an*la/lai(clayer)
	!sumaci(clayer)=sumaci(clayer)+an*ci
	!suma(clayer)=suma(clayer)+an
	sumet(clayer)=sumet(clayer)+et
	numsum(clayer)=numsum(clayer)+1.
ENDIF

END
!--------------------------------------------------------------------
REAL FUNCTION CDIFF(xmid)
! difference between metabolic assimilation rate and diffusive assimilation rate
USE CUP
USE METEO
USE METAB
implicit none
real FARQUHAR,DIFFUSION,COLLATZ,VONCAEMMERER
real xmid,anx,adx 

PATHWAY: SELECT CASE (c3path)
! Farqhuar C3 photosynthesis model (Farqhuar and Cowan, 1977)
	CASE(1)
		anx = FARQUHAR(vcmax,vjmax,kc,ko,gamma,resp,par,xmid)
! Collatz C4 photosynthesis model (Collatz et al. 1992)
	CASE(2)			
		anx = COLLATZ(vcmax,resp,par,xmid)
! von Caemmerer C4 photosynthesis model (von Caemmerer and Furbank, 1999)
	CASE(3)
		anx = VONCAEMMERER(vcmax,vpmax,vjmax,kc,ko,kp,resp,par,xmid)
	CASE DEFAULT
		WRITE(*,*) '--- *** LEAF ERROR! *** ---'
		WRITE(*,*) 'No model selected - check input veg file'
		WRITE(*,*) ''
		WRITE(*,*) 'PHYN_MOD must have values of 1,2 or 3'
		PAUSE '----------------------------'
		STOP
END SELECT PATHWAY

adx=diffusion(gs2,xmid,et,gbb,lt)
IF(anx.lt.0.)anx=0.
cdiff=adx-anx

END
! --------------------------------------------------------------------
REAL FUNCTION STOMDIFF(gs)
! efficiency check and caviation check to determine maximum gs
USE METAB
USE METEO
USE VEG
implicit none
real gs
real delta,ci2,ci1,et1,et2,an2,an1,eff,minpsi,lwp,iotac4
real stomata

delta=0.00003
lwp=psil			! hold initial psil value

ci2=stomata(gs-delta,lwp)	
an2=an				! uptake at lower gs

lwp=psil			! reinitialise psil value
ci1=stomata(gs,lwp)
an1=an				! uptake at gs

!eff=an1/an2-iota   ! efficiency check  old method

IF( c3path.EQ.1	) THEN
	eff = (an1-an2) - (C3iota-1.0)   ! efficiency check C3 plants
ELSE
	eff = (an1-an2) - (C4iota-1.0)   ! efficiency check C4 plants
ENDIF

IF( c3path.EQ.1	) THEN
ELSE
ENDIF

minpsi=lwp-minlwp	! cavitation check (NB altered)
!IF(minpsi.lt.0.)minpsi=0.	!to avoid bracketing error, keep minspi from going negative
! if either eff or minpsi are negative, then stomata is negative.  stomata only open if both checks are positive			
stomdiff=min(eff,minpsi)	
!stomdiff=eff
	
END

! --------------------------------------------------------------------
REAL FUNCTION STOMATA(gs,lwp)		
! Determines stable ci for given gs, net assimilation rate, diffusion rate, mitochondrial respiration,
! leaf temperature and leaf evaporation

USE VEG
USE CUP
USE METAB
USE METEO

IMPLICIT NONE
	REAL :: netrad,vcmt,vjmt,x1,x2,xacc,dpsildt,gs,boltz,lwp,ad,secs,	&
			Q10cp, Q10o
! Declare functions
	REAL :: LEAFTEMP, EVAP, TEMPMET, ARRHENIOUS, FARQUHAR, ZBRENT,		&
			CDIFF, DIFFUSION, COLLATZ, VONCAEMMERER, ARRHENIUS_MASSAD07	
	EXTERNAL :: CDIFF

	gs2 = gs			! swap argument gs into common block gs2 (does fuck all)
	boltz = 5.6703e-11	! boltzman constant kW m-2 K-4

! Calculate leaf temperature based on this gs
	lt = LEAFTEMP(gs)
! Estimate net radiation from net isothermal and temperature difference (Jones, p.108)	
	netrad = rad-4.*0.96*boltz*(temp+273.15)**3*(lt-temp)	
! Evaporation rate in g m-2 s-1, converted to clayer total mol m-2 s-1	
	et = MAX( 0.0, EVAP(gs,lt,netrad,wdef,gbb)*1.0/18.0 )		
! Calculate mitochondrial respiration	
	resp = rn*nit*EXP(log(2.0)*(lt-10.0)/10.0)

! Set parameters for CDIFF
	x1 = co2amb
	x2 = 0.1
	xacc = 0.0001

	Q10cp=2.1;	Q10o=1.2	! Q10 temperature responses for C4 K values (Chen et al. 1994)
	
! Calculate assimilation rate based on user declared photosynthesis model		
	PATHWAY: SELECT CASE (c3path)
! Farqhuar C3 photosynthesis model (Farqhuar and Cowan, 1977)
	CASE(1)
		vcmax = vcm*TEMPMET(65.03, opttemp, Vckurtosis, lt)
		vjmax = vjm*TEMPMET(57.05, opttemp, Vjkurtosis, lt)
		
		kc = ARRHENIOUS(310.0, 23.956, lt) ! (umol mol-1)
		ko = ARRHENIOUS(155.0, 14.509, lt) ! (mmol mol-1)
		gamma = ARRHENIOUS(36.5, 9.46, lt) ! (umol mol-1)

		ci = ZBRENT(CDIFF,x1,x2,xacc)
		an = FARQUHAR(vcmax,vjmax,kc,ko,gamma,resp,par,ci)
		
! Collatz C4 photosynthesis model (Collatz et al. 1992)
	CASE(2)
		vcmax = vcm*ARRHENIUS_MASSAD07(lt, 67294.0, 144568.0, 472.0)		! Arrhenius (Ea=67294,Hd=144568,DS=472) - Massad et al. (2007)

		ci = ZBRENT(CDIFF,x1,x2,xacc)		
		an = COLLATZ(vcmax,resp,par,ci)

! von Caemmerer C4 photosynthesis model (von Caemmerer and Furbank, 1999)
	CASE(3)
	! Vmax's are manipulated by leaf temperature - Arrhenius equation used
		vcmax = vcm*ARRHENIUS_MASSAD07(lt, 67294.0, 144568.0, 472.0)		! Arrhenius (Ea=67294,Hd=144568,DS=472) - Massad et al. (2007)
		vpmax = vpm*ARRHENIUS_MASSAD07(lt, 70373.0, 117910.0, 376.0)		! Arrhenius (Ea=70373,Hd=117910,DS=376)
		vjmax = vjm*ARRHENIUS_MASSAD07(lt, 77900.0, 191929.0, 627.0)		! Arrhenius (Ea=77900,Hd=191929,DS=627) 
	
	! Temperature responses of K parameters are derived from a Q10 function (K values from von Caemmerer & Furbank, (1999))
	! These values are for tobacco....
		kc = 650.0*Q10cp**(lt-25.0)/10.0				
		kp = 80.0 !*Q10o**(lt-25.0)/10.0	! Q10 causes ZBRENT problems for some reason...
		ko = 450.0*Q10cp**(lt-25.0)/10.0
		
		ci = ZBRENT(CDIFF,x1,x2,xacc)		
		an = VONCAEMMERER(vcmax,vpmax,vjmax,kc,ko,kp,resp,par,ci)

! If the correct model hasn't been stated then stop program		
	CASE DEFAULT
		WRITE(*,*) '--- *** LEAF ERROR! *** ---'
		WRITE(*,*) 'No model selected - check input veg file'
		WRITE(*,*) ''
		WRITE(*,*) 'PHYN_MOD must have values of 1,2 or 3'
		PAUSE '----------------------------'
		STOP
	END SELECT PATHWAY

	ad = DIFFUSION(gs,ci,et,gbb,lt)

	CALL DELTAPOTENTIAL(lwp)	! Change in leaf water potential

	STOMATA = ci

END FUNCTION STOMATA
!-------------------------------

SUBROUTINE DELTAPOTENTIAL(leafwp)	!change in leaf water potential
USE LWPINTEGRATOR
implicit none
integer time
INTEGER NVAR,i
PARAMETER (NVAR=1)
INTEGER kmax,kount,nbad,nok
REAL dxsav,eps,h1,hmin,x(KMAXX),y(NMAX,KMAXX),x1,x2
real dstore_dt,ystart(nvar),leafwp
EXTERNAL lwp,rkqs
COMMON /path/ kmax,kount,dxsav,x,y
eps=1.0e-4; h1=.01; hmin=0.0; kmax=100; x1=1.; x2=2.
dxsav=(x2-x1)/20.0
ystart(1)=leafwp	!initial conditions

CALL odeint(ystart,nvar,x1,x2,eps,h1,hmin,nok,nbad,lwp,rkqs)

!write(*,'(/1x,a,t30,i3)') 'Successful steps:',nok
!write(*,'(1x,a,t30,i3)') 'Bad steps:',nbad
!write(*,'(1x,a,t30,i3)') 'Stored intermediate values:',kount
leafwp=ystart(1)

END

!-------------------------------
SUBROUTINE LWP(time,y,dydt)	!differential equation describing change in leaf water potential given supply & demand
USE meteo
USE metab
use canopy
use lwpintegrator
implicit none
integer i
real dydt(NMAX),y(NMAX),numsecs
real time
real wetevap !function
real secs,add_store,drain_store,add_ground,potential_evap,ratio,b,a

secs=24./steps*3600.	! # 0f seconds per timestep
dydt(1)=secs*(psis-(head*ht)-1000.*la*et*(rplant+rsoil)-y(1))/(layer_capac*(rplant+rsoil))

!y(1) = psil, leaf water potential
END
!--------------------------------------------------------------------

REAL FUNCTION FARQUHAR(Vcmax,Jmax,Kc,Ko,gammastar,Rd,PAR,Ci) 
! Metabolic C3 assimilation rate according to Farquhar and von Caemmerer 1982

	IMPLICIT NONE

	REAL,INTENT(IN) :: Vcmax, Jmax, Kc, Ko, gammastar, Rd, PAR, Ci
	
	REAL :: Oi, alphaj, theta, Wc, Wj, J, Vc, An
	REAL :: QUADRATIC
	
	Oi = 210.0			! Internal CO2 concentration
	alphaj = 0.385		! Initial slope of quantum response curve
	theta = 0.7			! Curvature of quantum response curve

! Determine Rubisco limited carboxylation rate
	Wc = (Vcmax*Ci)/(Ci+Kc*(1.0+Oi/Ko))	
! Determine potential rate of RuBP regeneration
	J = QUADRATIC(theta,-(alphaj*PAR+Jmax),alphaj*PAR*Jmax,-1.0)
! Determine RuBP regeneration limited carboxylation rate
	Wj = J*Ci/(4.5*Ci+10.5*gammastar)		
! Determine limiting carboxylation rate
	Vc = MIN(Wc,Wj)						
! Net photosynthetic rate 
	An = (Vc*(1.0-gammastar/Ci)-Rd)			
! Return answer to user	
	FARQUHAR = An							

END FUNCTION FARQUHAR

!--------------------------------------------------------------------

REAL FUNCTION COLLATZ(Vcmax,Rd,PAR,Ci)
! Metabolic C4 assimilation rate from Collatz et al. 1992

IMPLICIT NONE

	REAL,INTENT(IN) :: Vcmax, Rd, PAR, Ci

	REAL :: alpharf, theta, beta, K, A, An, M
	REAL :: QUADRATIC
	
	alpharf = 0.067			! mol/mol
	K = 0.7					! mol/m2/s
	theta = 0.83			! Curvature of quantum response curve, Collatz table2
	beta  = 0.93			! Collatz table 2

! Rubisco and light limited capacity
	M = QUADRATIC(theta,-(alpharf*PAR+Vcmax),alpharf*PAR*vcmax,-1.0)	
! M and CO2 limitation
	A = QUADRATIC(beta,-(M+K*Ci),M*K*Ci,-1.0)
! Net photosynthetic rate 	
	An = A-Rd
! Return answer to user		
	COLLATZ = An

END FUNCTION COLLATZ

!--------------------------------------------------------------------

REAL FUNCTION VONCAEMMERER(Vcmax,Vpmax,Jmax,Kc,Ko,Kp,Rd,PAR,Ci)
! C4 assimilation rate is calucated using quadratic solutions for the enzyme and light
! limited rates of photosynthesis. All values and calculations are take from the
! von Caemmerer and Furbank (1999) paper.
! *Note: We express that Ci is equivalent to Cm

IMPLICIT NONE
! Function inputs
	REAL, INTENT(IN) :: Vcmax, Vpmax, Jmax,	&
						Kc, Ko, Kp,				&
						Rd, PAR, Ci
! Function variables					
	REAL :: a_c, b_c, c_c, a_j, b_j, c_j,		&
			gbs, low_gamstar, Oi, K, Rm, alpha, &
			Vp, Vpr, J, Q2, shape, x,			&
			A_enzyme, A_light, A_actual
	
	REAL :: QUADRATIC 
	
	alpha = 0.0				! Degree of PSII activity occuring in the bundle sheath cells (adjust between 0-1)
	Oi = 210.0				! Intercellular O2 equivalent to Om
	gbs = 0.003				! Bundle sheath conductance (mol m-2 s-1)
	low_gamstar = 1.93E-4	! Half the reciprocal for Rubisco specificity (NOT CO2 compensation point)
	x = 0.4					! Partitioning of the electron transport between mesophyll and bundle sheath cells
	shape = 0.7				! Shape of the hyperbolic response curve (typical value)
	Rm = 0.5*Rd				! Mitchondrial respiration from the mesophyll cells is half of Rd
	Vpr = 80.0				! PEP limiting regeneration rate
	K = Kc*(1.0+Oi/Ko)		! Combined Michelson-Menton constant for Ci and Oi

! PEP carboxylation rate
	Vp = MIN(Ci*Vpmax/(Ci+Kp),Vpr)	

! Quadratic solution for enzyme limited C4 assimilation
	a_c = 1.0 - (alpha/0.047)*(Kc/Ko)
	b_c = -( (Vp-Rm+gbs*Ci) + (Vcmax-Rd) + gbs*K + alpha*low_gamstar/0.047*( low_gamstar*Vcmax+Rd*Kc/Ko ) )
	c_c = (Vcmax-Rd)*(Vp-Rm+gbs*Ci) - (Vcmax*gbs*low_gamstar*Oi + Rd*gbs*K)
	
	A_enzyme = QUADRATIC(a_c,b_c,c_c,-1.0)	! Using negative quadratic solution
	
! Non-rectangular hyperbola describing light effect on electron transport rate (J)
	Q2 = PAR*(1.0-0.15)/2.0
	J = QUADRATIC(shape,-(Q2+Jmax),(Q2*Jmax),-1.0)
	
! Quadratic solution for light-limited C4 assimilation       
	a_j = 1.0 - 7.0*low_gamstar*alpha/(3.0*0.047)
	b_j = -( (x*J/2.0-Rm+gbs*Ci) + ((1.0-x)*J/3.0-Rd) + gbs*(7.0*low_gamstar*Oi/3.0)	&
			+ alpha*low_gamstar/0.047*((1.0-x)*J/3.0+Rd) )
	c_j = ( (x*J/2.0-Rm+gbs*Ci)*((1.0-x)*J/3.0-Rd) - gbs*low_gamstar*Oi*((1.0-x)*J/3.0-7.0*Rd/3.0) )
	
	A_light = QUADRATIC(a_j,b_j,c_j,-1.0)	! Using negative quadratic solution
	
! The minimum between enzyme and light limited respiration rates is the actual rate
	A_actual = MIN(A_enzyme,A_light)
! Return answer to user	
	VONCAEMMERER = A_actual
	
END FUNCTION VONCAEMMERER

!--------------------------------------------------------------------

REAL FUNCTION DIFFUSION(gs,ci,e,gbbb,ttemp)
!  diffusion limited assimilation rate
USE VEG
USE METEO
implicit none
real gs,ad,e,gbbb,ttemp
real convert,gt,ca,ci

ad=0.0
ca=co2amb
! see Jones Appendix 3 for conversion ms-1 to mol s-1)
convert=atmos_pres/(Rcon*(273.15+ttemp)) ! and Jones Appendix 2 for ratio CO2 diffusion to water diffusion (=1.65)
gt=convert/(1.65/gs+1.37/gbbb+1./gi)! total leaf conductance (converts from ms-1 to mols-1)
! interaction between water flux out of and CO2 flux into the stomata
! determines diffusion limited assimilation rate
ad=(gt-0.5*e)*ca-(gt+0.5*e)*ci
diffusion=ad
END

!--------------------------------------------------------------------
REAL FUNCTION EVAP(gs,tt,q,wdef,gbb)
!  determine evapotranspiration rate (g m-2 s-1) from q (kW m-2), tt (oC), wdef (g m-3) and gbb and gs (m s-1)
USE VEG
implicit none
real gs,epsilon,lambda,tt,gleaf,q,wdef
real s,slope,psych,gbb,gcut
gcut=0.0005
gleaf=gcut+gs
s=6.1078*17.269*237.3*exp(17.269*tt/(237.3+tt))	! slope of saturation vapour pressure curve (t-dependent)
slope=0.1*(s/(237.3+tt)**2)			! kPa K-1
psych=0.1*(0.646*exp(0.00097*tt))	! psych is temp-dependent (kPa K-1)
epsilon=slope/psych					! response of epsilon to temp
lambda=0.001*(2501.0-2.364*tt)		! latent heat of vapourisation (KJ g-1)
evap=(epsilon*q/lambda+wdef*gbb)/(1.0+epsilon+gbb/gs)	!g m-2 s-1

END
!--------------------------------------------------------------------
REAL FUNCTION LEAFTEMP(gs)
! determines leaf temperature 
! corrected calculation of s, 25 Feb 1998 - matches that in EVAP above.
USE VEG
USE METEO	!includes la (leaf area for the clayer) and temp,wdef,rad,gbb
USE ERROR
implicit none
real lambda,Q,gs,rt,denom,tt,gr,ghr
real slope,s,rho,ta,emiss,boltz,de,psych,diff,rhr

tt=temp
q=rad
emiss=0.96
boltz=5.6703e-11	! SB constant KW m-2 K-4
ta=tt+273.15
rho=353000.0/ta							! density of air g m-3 (t-dependent)
s=6.1078*17.269*237.3*exp(17.269*tt/(237.3+tt))	! slope of saturation vapour pressure curve (t-dependent)
slope=0.1*(s/(237.3+tt)**2)
psych=0.1*(0.646*exp(0.00097*tt))		! psych is temp-dependent 
lambda=0.001*(2501.0-2.364*tt)			! latent heat of vapourisation
de=wdef*lambda*psych/(rho*cp)			! convert water deficit to vapour pressure deficit
!gr=(1.-exp(-0.5*la))*4.*emiss*boltz*ta**3/(rho*cp)	!   radiative heat conductance gR (Jones p. 108)  gr=4.*emiss*boltz*ta**3/(rho*cp)
gr=4.*emiss*boltz*ta**3/(rho*cp)	!  remove leaf area sensitivity from gr
ghr=gr+2.*gbb/0.93						! total thermal conductance
rhr=1./ghr
rt=1.0/gs+1.0/gbb						! combined leaf resistance
denom=psych*rt+slope*rhr
diff=rhr*rt*psych*q/(rho*cp*denom)-rhr*de/denom		! temperature difference
leaftemp=diff+tt						! leaf temp
IF(abs(diff).gt.10.)THEN
!	write(*,*)leaftemp,tt,rad,wdef,' high temp'
!	write(*,'(I6,2x,F8.2,2x,I6))')errday,errtime,errlayer
!	pause 'leaf temp problem'
    temperror=1.		!record a leaf temp problem
ENDIF

END
!-------------------------------------------------------------------

real function ARRHENIOUS(a,b,t)
real a,b,t
arrhenious=a*exp(b*(t-25.0)/(t+273.2))
end

!-------------------------------------------------------------------

REAL FUNCTION ARRHENIUS_MASSAD07(lt,Ea,Hd,DS)
! Arrhenius function use with C4 metabolic parameters as described by
! Massad et al. 2007

IMPLICIT NONE

	REAL,INTENT(IN) :: lt, Ea, Hd, DS
	REAL ::	R, Tk, fTk
	
	R = 8.3144			! Universal gas constant
	Tk = lt + 293.0		! Convert leaf temperature from degrees to Kelvin
	
! Arrhenius expression as used by Massad et al. 2007	
	fTk = EXP( Ea*(Tk-298.0)/(298.0*R*Tk) )				&
			* (1.0 + EXP( (298.0*DS-Hd)/(298.0*R) ))	&
			/ (1.0 + EXP( (Tk*DS-Hd)/(Tk*R) )) 

! Return answer to user						
	ARRHENIUS_MASSAD07 = fTk
	
END FUNCTION ARRHENIUS_MASSAD07

!-------------------------------------------------------------------

REAL FUNCTION QUADRATIC(a, b, c, sign) 
! Generic quadratic solution function
IMPLICIT NONE
	
	REAL,INTENT(IN) :: a, b, c, sign
	REAL :: x
	
	IF(sign < 0.0) THEN
		x = (-b-SQRT(b**2.0-4.0*a*c))/(2.0*a)	! Negative quadratic equation
	ELSE
		x = (-b+SQRT(b**2.0-4.0*a*c))/(2.0*a)	! Positive quadratic equation
	END IF
	
! Return answer to user		
	QUADRATIC = x

END FUNCTION QUADRATIC

!-------------------------------------------------------------------
real function TEMPMET(max,opt,q,x)
implicit none
real max,opt,q,x,dum
if (x.ge.max)then
         tempmet=0.0
else
         dum=(max-x)/(max-opt)
         dum=exp(log(dum)*q*(max-opt))
         tempmet=dum*exp(q*(x-opt))
endif
   
end
! --------------------------------------------------------------------
REAL FUNCTION COMP(ccc,g1)
! determine light-limitation to photosynthesis
USE METEO
USE METAB
implicit none
real farquhar,tempmet,arrhenious,leaftemp,collatz    !functions
real vcmt,vjmt,resx,ccc,g1,gs,vcmax,vjmax,kc,ko,gamma,lt

!  {temperature modifications begin below: based on air - not leaf - temperature}
gs=g1
lt=leaftemp(gs)
vcmt=tempmet(65.03,opttemp,Vckurtosis,lt)	!  {temperature modifications begin below}
vjmt=tempmet(57.05,opttemp,Vjkurtosis,lt)
vcmax=vcmt*vcm
vjmax=vjmt*vjm
kc=arrhenious(310.0,23.956,lt)
ko=arrhenious(155.0,14.509,lt)
gamma=arrhenious(36.5,9.46,lt)
resx=rn*nit*exp(log(2.0)*(lt-10.0)/10.0)

IF(c3path.eq.1)THEN		!c3
	comp=farquhar(vcmax,vjmax,kc,ko,gamma,resx,par,ccc)   
ELSE
	comp=collatz(vcmax,resx,par,ccc)
ENDIF


END
! --------------------------------------------------------------------
SUBROUTINE MINSTOM(lowg,maxg)		!minimum gs for net C fixation, max gs in drough situation
USE METAB
USE METEO
USE VEG
implicit none
real low,high,xacc,mings,lwp,maxg,lowg
real leafcp,zbrent,stomata			!declare functions
EXTERNAL leafcp
an=0.
low=0.00002
high=0.05; maxg=high
xacc=0.00000001
lwp=psil
ci=stomata(low,lwp)
IF((an.gt.0.).or.(temp.lt.5.))THEN
	mings=0.00005
ELSE
	mings=zbrent(LEAFCP,low,high,xacc)  !add delta
	mings=mings+0.00004
ENDIF
! check lwp limit
lwp=psil
ci=stomata(mings,lwp)
IF(lwp.lt.minlwp)THEN
	maxg=low
ENDIF
lowg=mings
END
! --------------------------------------------------------------------
REAL FUNCTION LEAFCP(gs)		! leaf compensation point
USE METAB
USE METEO
implicit none
real gs,lwp
real stomata				! declare functions
lwp=psil
ci=stomata(gs,lwp)
leafcp=an
END


! --------------------------------------------------------------------
SUBROUTINE LOCATE_ERROR()
USE ERROR
USE METEO
USE METAB
USE VEG
implicit none
write(*,'(I6,2x,F8.2,2x,I6)')errday,errtime,errlayer
write(*,*)lowg,lcp
write(*,*)"temp,wdef,rad,gbb,par,psil,psis,nit,la,co2amb"
write(*,'(10(F8.4,2x))')temp,wdef,rad,gbb,par,psil,psis,nit,la,co2amb
!write(*,*)temp,wdef,rad,gbb,par,psil,psis,nit,la

END


REAL FUNCTION VONCAEMMERER2(Vcmax,Vpmax,Jmax,Kc,Ko,Kp,Rd,PAR,Ci)
! ** This is the simpler expression for C4 photosynthesis as derived by von Caemmerer
!    Just replace the function in the STOMATA subroutine if you wish to use it. However
!    I see little difference in the results between the two.
!
! C4 assimilation rate is calucated using quadratic solutions for the enzyme and light
! limited rates of photosynthesis. All values and calculations are take from the
! von Caemmerer and Furbank (1999) paper.
! *Note: We express that Ci is equivalent to Cm

IMPLICIT NONE
! Function inputs
	REAL, INTENT(IN) :: Vcmax, Vpmax, Jmax,	&
						Kc, Ko, Kp,				&
						Rd, PAR, Ci
! Function variables					
	REAL :: a_c, b_c, c_c, a_j, b_j, c_j,		&
			gbs, low_gamstar, Oi, K, Rm, alpha, &
			Vp, Vpr, J, Q2, shape, x,			&
			A_enzyme, A_light, A_actual
	
	REAL :: QUADRATIC 
	
	alpha = 0.3				! Degree of PSII activity occuring in the bundle sheath cells (adjust between 0-1)
	Oi = 210.0				! Intercellular O2 equivalent to Om
	gbs = 0.003				! Bundle sheath conductance (mol m-2 s-1)
	low_gamstar = 1.93E-4	! Half the reciprocal for Rubisco specificity (NOT CO2 compensation point)
	x = 0.4					! Partitioning of the electron transport between mesophyll and bundle sheath cells
	shape = 0.7				! Shape of the hyperbolic response curve (typical value)
	Rm = 0.5*Rd				! Mitchondrial respiration from the mesophyll cells is half of Rd
	Vpr = 80.0				! PEP limiting regeneration rate
	K = Kc*(1.0+Oi/Ko)		! Combined Michelson-Menton constant for Ci and Oi

! PEP carboxylation rate
	Vp = MIN( (Ci*Vpmax/(Ci+Kp)), Vpr )	
	
	A_enzyme = MIN( (Vp-Rm+gbs*Ci), (Vcmax-Rd) )
	
! Non-rectangular hyperbola describing light effect on electron transport rate (J)
	Q2 = PAR*(1.0-0.15)/2.0
	J = QUADRATIC(shape,-(Q2+Jmax),(Q2*Jmax),-1.0)
		
	A_light = MIN( (x*J/2-Rm+gbs*Ci), ((1-x)*J/3-Rd) )
	
! The minimum between enzyme and light limited respiration rates is the actual rate
	A_actual = MIN(A_enzyme,A_light)
! Return answer to user	
	VONCAEMMERER2 = A_actual
	
END FUNCTION VONCAEMMERER2

!--------------------------------------------------------------------
