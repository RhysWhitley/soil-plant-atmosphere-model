MODULE CANOPY       ! These declarations define the size of the soil and canopy profiles, and the time resolution
INTEGER numl, steps, layer, core
PARAMETER(numl=10, steps=48)	! Number of canopy layers, and number of timesteps per day
PARAMETER(layer=20)			! Soil layer 
PARAMETER(core=21)			! Soil layers + 1
END MODULE


