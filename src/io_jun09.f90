! Input and output files for SPA

SUBROUTINE INIT(initialwater, meto_file, phen_file, vege_file, soil_file)
	! open files, read data, write headers
	USE CLIM
	USE VEG
	USE METEO
	USE SOIL_STRUCTURE
	USE HYDROL
	USE HOURSCALE
	USE SNOWINFO
	USE METAB
	USE C13
	
	IMPLICIT NONE
	INTEGER :: i
	REAL :: initialwater, psisoil, rootxsecarea
	CHARACTER*60 header, meto_file, vege_file, phen_file, soil_file

    ! open input files to be read
    OPEN(UNIT=23, FILE=meto_file, status='old')
	OPEN(UNIT=25, FILE=vege_file, status='old')
	OPEN(UNIT=26, FILE=phen_file, status='old')
	OPEN(UNIT=27, FILE=soil_file, status='old') 
	
	!open output files
	OPEN(UNIT=30,FILE='outputs/daily.csv',status='unknown')
	OPEN(UNIT=31,FILE='outputs/drivers.csv',status='unknown')
	OPEN(UNIT=35,FILE='outputs/hourly.csv',status='unknown')
	OPEN(UNIT=51,FILE='outputs/solar.csv',status='unknown')
	OPEN(UNIT=52,FILE='outputs/power.csv',status='unknown')
	OPEN(UNIT=60,file='outputs/soilwater.csv',status='unknown')
	OPEN(UNIT=62,FILE='outputs/upfrac.csv',status='unknown')
	OPEN(UNIT=63,FILE='outputs/soilevap.csv',status='unknown')
	OPEN(UNIT=64,FILE='outputs/parcheck.csv',status='unknown')
	OPEN(UNIT=68,file='outputs/iceprop.csv',status='unknown')
	OPEN(UNIT=70,FILE='outputs/ci.csv',status='unknown')
	OPEN(UNIT=83,FILE='outputs/soiltemp.csv',status='unknown')
	OPEN(UNIT=90,FILE='outputs/test.csv',status='unknown')	!extra output for testing C4 module - can be removed
	OPEN(UNIT=94,FILE='outputs/waterfluxes.csv',status='unknown')
	OPEN(UNIT=101,FILE='outputs/soilstatus.csv',status='unknown')

	! WRITE HEADERS TO OUTPUT FILES
	WRITE(30,*) 'time, et, modle, gpp, swp, rplant, soilres, lsc, sapflow'
	WRITE(31,*) 'time, lai, maxt, mint, rad, minlwp, co2av, maxvpd, wspd, psis'	
	WRITE(35,*) 'time, gpp, resp, lemod, transle, soille, wetle, sapflow'
	WRITE(60,965)
		965 FORMAT('time, w1, w2, w3, w4, w5, w6, w7, w8, w9, w10, w11, w12, w13, w14, w15, w_swp')
	WRITE(62,966)
		966 FORMAT('time, up1, up2, up3, up4, up5, up6, up7, up8, up9, up10, up11, up12, up13, up14, up15')
	WRITE(68,967)
		967 FORMAT('time, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15')	
	WRITE(83,968)
		968 FORMAT('time, st1, st2, st3, st4, st5, st6, st7, st8, st9, st10, st11, st12, st13, st14, st15, core')
	WRITE(94,969)
		969 FORMAT('time, modess, runoff, ppt, trans, delw, disc, cwtr, check, diff, modwet, unint, canst')
	WRITE(101,970)
		970 FORMAT('time, of, totet, uf, sin, wtr, wch, flux, chk, w+1, w-1, delw, chkdff, ppt+, w+, w-, w+c, w-c, snwt, snht')
    WRITE(52,974)
        974 FORMAT('time, longdown, skylong, longnet, netrad, abspar')


	read(23,*)header
	read(26,*)header

	! read soil properties
	read(27,*)header
	read(27,*)header,(thickness(i),i=1,core)
	read(27,*)header,(organicfrac(i),i=1,core)
	read(27,*)header,(mineralfrac(i),i=1,core)
	read(27,*)header,(waterfrac(i),i=1,core)
	read(27,*)header,(soiltemp(i),i=1,core)
	read(27,*)header,(iceprop(i),i=1,core)
	read(27,*)header,(rootfrac(i),i=1,core)		! root biomass fraction in each layer
	read(27,*)header,rootl						! number of soil layers with roots
	read(27,*)header,rootbiomass				! initial total biomass of root in all layers
	read(27,*)header,(sandpc(i),i=1,core)		! Does sand vary with all layers
	read(27,*)header,(claypc(i),i=1,core)		! Does clay vary with all layers
	read(27,*)header,rootrad
	read(27,*)header,(draincheck(i),i=1,core)	! Does draincheck vary with all layers
	!read(27,*)header,draincheck(1)				! Is draincheck constant for all layers
	read(27,*)header,snowweight
	read(27,*)header,snowheight

	!draincheck=draincheck(1)	!set parameter constant for all soil layers
	!sandpc=sandpc(1)			!set parameter constant for all soil layers
	!claypc=claypc(1)			!set parameter constant for all soil layers

    ! put thicknesses at top of file for integration purposes
    WRITE(60,'(17(f7.6,","))') -9999., (thickness(i),i=1,15), -9999.
    WRITE(62,'(17(f7.6,","))') -9999., (thickness(i),i=1,15)
    WRITE(68,'(17(f7.6,","))') -9999., (thickness(i),i=1,15)
    WRITE(83,'(17(f7.6,","))') -9999., (thickness(i),i=1,15), -9999.

	initialwater=0.
	DO i=1,layer
		initialwater=initialwater+1e3*(waterfrac(i)*thickness(i))
	ENDDO

	rootmass=0.
	DO i=1,rootl
		rootmass(i)=rootfrac(i)*rootbiomass/thickness(i)
	ENDDO
	rootxsecarea=3.14159*rootrad**2	!root X-sectional area (m2)

	DO i=1,rootl	!root length density in each rooted soil layer
		rootlength(i)=rootmass(i)/(rootdens*rootxsecarea)	!m m-3 soil
	ENDDO

	canopy_store=0.
	drythick=0.001
	hourts=soiltemp(1)	!initial soil temp estimate for light routine
	wettingbot(1)=thickness(1)	!top layer is saturated

	saxton=1			!IF(saxton.eq.0) THEN turn off Saxton water potential
	gi=1.				!v large mesophyll conductance - ACi curves have not been Epron adjusted

	!read vegetation parameters
	read(25,*)header,(layerht(i),i=1,numl)	!layer heights
	read(25,*)header,(c3(i),i=1,numl)		!PS pathway; c3=1, c4=2,3
	read(25,*)header,totla			! initial total leaf area index
	read(25,*)header,totn			! initial total foliar N, gN m-2 ground
	read(25,*)header,gplant			! conductivity
	read(25,*)header,minlwp			! critical LWP
	read(25,*)header,C3iota			! stomatal efficiency for C3 plants
	read(25,*)header,C4iota			! stomatal efficiency for C4 plants
	read(25,*)header,capac			! leaf capacitance	
	read(25,*)header,lat			! latitude
	read(25,*)header,outt			! detailed output?
	read(25,*)header,dimen			! leaf size
	read(25,*)header,rootresist		! root resistivity
	read(25,*)header,towerht		! height of measurement tower
	read(25,*)header,conductivity	! Does conductance vary with stem length? 0=NO, 1=YES
	read(25,*)header,C3KAP_VC		! Metabolic constant for C3 layers - determines Vcmax
	read(25,*)header,C3KAP_VJ		! Electron transport constant for C3 layers - determines Jmax
	read(25,*)header,C4KAP_VC		! Metabolic constant for C4 layers - determines Vcmax
	read(25,*)header,C4KAP_VP		! Metabolic constant for C4 layers - determines Vpmax
	read(25,*)header,C4KAP_VJ		! Electron transport constant for C4 layers - determines Jmax

	canht=layerht(1)
	lat=lat*3.14159/180.	!convert degrees to radians
	atmos_pres=100000.		!estimated atmospheric pressure at 1000 m altitude

	psisoil=0.
	!on day 1 set leaf WP in each layer
	DO i=1,numl
		LWPstore(i)=psisoil-head*layerht(i)	! initial leaf water potential = predawn WP = soil WP adjusted for height
	ENDDO


	IF(outt.eq.1.)THEN	! what is going on here???? why here and not before???
		OPEN(unit=40,file='outputs/energy.csv',status='unknown')
		WRITE(40,*)'time,Qh,Qe,Qn,Qc,airt,surfacet,soilt2,drythick'

		OPEN(UNIT=41,FILE='outputs/l1.csv',status='unknown')
		OPEN(UNIT=42,FILE='outputs/l2.csv',status='unknown')
		OPEN(UNIT=43,FILE='outputs/l3.csv',status='unknown')
		OPEN(UNIT=44,FILE='outputs/l4.csv',status='unknown')
		OPEN(UNIT=45,FILE='outputs/l5.csv',status='unknown')
		OPEN(UNIT=46,FILE='outputs/l6.csv',status='unknown')
		OPEN(UNIT=47,FILE='outputs/l7.csv',status='unknown')
		OPEN(UNIT=48,FILE='outputs/l8.csv',status='unknown')
		OPEN(UNIT=49,FILE='outputs/l9.csv',status='unknown')
		OPEN(UNIT=50,FILE='outputs/l10.csv',status='unknown')

		DO i=1,numl
			WRITE(40+i,971)
			971 FORMAT('Time, gs, gs.Sun, gs.Shade, A, A.Sun, A.Shade, Rd, lwp, Ci, Ca, Et, WDef, PAR, Rn, Tleaf, Tair, gb, LAI')
		ENDDO
	ENDIF

	!set C13 counters to zero
	sumaci=0.
	suma=0.
	sumet=0.
	numsum=0.

	RETURN

END SUBROUTINE INIT

!-------------------------------------------------------------------

SUBROUTINE IO_DAILY()
! read climate data and vegetation data, updates phenology
USE CLIM
USE VEG
USE HOURSCALE
USE SOIL_STRUCTURE
implicit none
integer i, j
real vpdtop,vpdbot,laitree,rootxsecarea

gppt=0.; respt=0.; transt=0.		! set counters to zero
totass=0.; totres=0.; totevap=0. ; wetev=0.
runoff=0.; discharge=0.; flux=0.

DO i=1,steps	  !met data
	read(23,*)daytime(i),temptop(i),coa(i),windsp(i),swrad(i),vpdtop,partop(i),ppt(i)
	IF(windsp(i).lt.0.2)windsp(i)=0.2
	tempbot(i)=temptop(i)
	vpdbot=vpdtop
    wdtop(i)=vpdtop*217./(0.1*(temptop(i)+273.4))	!absolute water deficit in g m-3
    wdbot(i)=vpdbot*217./(0.1*(tempbot(i)+273.4))
ENDDO

! read phenology data
READ(26,*) daynum, rootbiomass, laitree, (lafrac(j),j=1,numl), totn, (nfrac(j),j=1,numl)
lai=lafrac*laitree		! assign leaf area to each layer
nla=totn*nfrac			! nitrogen per layer


rootmass=0.
DO i=1,rootl
	rootmass(i)=rootfrac(i)*rootbiomass/thickness(i)
ENDDO
rootxsecarea=3.14159*rootrad**2	!root X-sectional area (m2)

DO i=1,rootl	!root length density in each rooted soil layer
	rootlength(i)=rootmass(i)/(rootdens*rootxsecarea)	!m m-3 soil
ENDDO

RETURN
END

!-------------------------------------------------------------------
SUBROUTINE DATOUT(prevwater)
USE VEG
USE CLIM
USE SOIL_STRUCTURE
USE HOURSCALE
USE IRRADIANCE_SUNSHADE
USE METEO
implicit none
integer t,i,j
real por,ld,ts,co2av,wspd,totmodnee,totnee,countnee,hournee,hourle,mmmod(steps)
real totmodle,totle,countle,uu(steps),vv(steps),ubar,vbar,theta,nttemp,posess(steps)
real tempw,tmix,diff,et,lambda,nightflux,maxwd,direc,mrespsum,daytrans,modwetle
real gas,vpdtop,vpdbot,modnee,nee,modle,gppsum,respsum,soilsum,modess,soilLE
real neemod(steps),let(steps),le,lemod(steps),dayrad,maxtemp,mintemp,actgp,totalflux
real currentwater,delwater,prevwater,check,numsecs,wetle(steps),modwet,intrad,soilt,addup
real modess2,modess3,daymm,water3,water6,water10,water15,rplant,lsc,timeflux(steps),radab(steps)
real dayint		! function
REAL SIND, COSD, ATAND
por=0.4; ld=0.0024;ts=2.; countnee=0.; uu=0.; vv=0.;mmmod=0.
nightflux=3.0; totnee=0.; totmodnee=0.	;countle=0.
totle=0.; totmodle=0.;timeflux=0.
numsecs=3600.*24/real(steps)	!#secs per timestep
IF(sum(mresp).eq.0.)mresp=nightflux+respt


DO t=1,steps
	uu(t)=windsp(t)*cosd(winddir(t))
	vv(t)=windsp(t)*sind(winddir(t))
	lambda=1000.*(2501.0-2.364*temptop(t))	! latent heat of vapourisation, J kg-1
	wetle(t)=lambda*wetev(t)/numsecs	!wet leaves evap convert mm t-1 to Wm-2
	neemod(t)=-1.*gppt(t)+mresp(t)	!estimate NEE
	lemod(t)=transt(t)+ess(t)+wetle(t) !sum modelled canopy & modelled soil LE, and wet canopy evap
	mmmod(t)=transt(t)*numsecs/lambda+ess(t)*numsecs/lambda+wetle(t)*numsecs/lambda !sum modelled canopy & modelled soil LE, and wet canopy evap
	DO j=1,10	!sum flux from each tree layer 
		timeflux(t)=timeflux(t)+flux(t,j)	!mmol m-2 GA s-1
	ENDDO
	write(35,'(24(f9.3,","))')daytime(t),-1.*gppt(t),respt(t),lemod(t),transt(t),ess(t),wetle(t),timeflux(t)
ENDDO

radab=0.
If(outt.eq.1)THEN
	DO t=1,steps	!check that radiation routines are working.
		addup=0.
		DO i=1,numl
			radab(t)=radab(t)+parabs_sun(t,i)+parabs_shade(t,i)
			addup=addup+checkpar(t,i)
		ENDDO
		write(64,'(14(f12.2,","))')daytime(t),partop(t),radab(t),addup,(checkpar(t,i),i=1,numl)
	ENDDO
ENDIF


gppsum=dayint(gppt,0.)*1e-6*12.			! total GPP, gC m-2 d-1
respsum=dayint(respt,0.)*1e-6*12.		! total leaf resp, gC m-2 d-1
soilsum=nightflux*24.*60.*60*1e-6*12.	! total soil/stem resp, gC m-2 d-1
mrespsum=dayint(mresp,0.)*1e-6*12.		! total measured ecosystem respiration, gC m-2 d-1
modnee=mrespsum-gppsum					! total NEE, gC m-2 d-1
nee=dayint(fC,nightflux)*1e-6*12.		! gC m-2 d-1, measured C exchange
soille=dayint(Lesoil,0.)*1e-6			! MJ m-2 d-1, measured soil evaporation
modle=dayint(lemod,0.)*1e-6				! MJ m-2 d-1, modelled ecosystem water loss to atmosphere	
le=dayint(fW,0.)*1e-6					! MJ m-2 d-1, measured ecosystem water loss to atmosphere	
daytrans=dayint(transt,0.)*1e-6			! MJ m-2 d-1, modelled canopy transpiration
modess=sum(soiletmm)					! mm d-1, modelled soil evaporation
modess2=dayint(ess,0.)*1e-6				! MJ m-2 d-1, modelled soil evaporation
posess=max(0.,ess)						! remove dew component
modess3=dayint(posess,0.)*1e-6				! MJ m-2 d-1, modelled soil evaporation
dayrad=dayint(partop,0.)*1e-6/2.3		! MJ m-2 d-1 measured irradiance
modwet=sum(wetev)						! mm d-1, modelled evap from wetted canopy
modwetle=dayint(wetle,0.)*1e-6			! MJ m-2 d-1, modelled evap from wet canopy
daymm=sum(mmmod)						! mm d-1, total ET
maxtemp=maxval(temptop)
mintemp=minval(temptop)

totalflux=dayint(timeflux,0.)*0.001*18*0.001		! total flux  (mm d-1 or kg m-2 ga d-1)

soilt=sum(st)/real(steps)						!mean soil temperature
nttemp=sum(temptop(1:12))/12			!night time temperature
maxwd=maxval(wdtop)/217.*(0.1*(maxtemp+273.4))	!convert from g m-3 to KPa
co2av=sum(coa)/real(steps)				!mean CO2 conc.
IF(co2av.lt.100.)co2av=355.
wspd=sum(windsp)/24.
ubar=(sum(uu)/steps)
vbar=(sum(vv)/steps)
theta=atand(vbar/ubar)	!theta is mean daily wind direction
IF(ubar.lt.0.)THEN
	theta=theta+180.
ENDIF
IF(theta.lt.0.)THEN
	theta=theta+360.
ENDIF


!determine leaf specific conductance for 2nd canopy layer
IF(conductivity.eq.1)THEN
	rplant=layerht(2)/(gplant*lai(2))	!MPa s mmol-1 layer
ELSE
	rplant=1./(gplant*lai(2))		!conductance is constant with height
ENDIF
lsc=(1./(rplant+canopy_soil_resistance(2)))/lai(2)

write(30,'(16(f8.2,","))')daytime(1),daymm,modle,gppsum,weighted_SWP,rplant,canopy_soil_resistance(2),lsc,totalflux

write(31,'(25(f8.2,","))')daytime(1),sum(lai),maxtemp,mintemp,dayrad,minlwp,co2av,maxwd,wspd,psis	!daily drivers


currentwater=sum(watericemm)
delwater=currentwater-prevwater	!change in soil water storage (mm)
check=-1.*(modess+runoff*1e3+totevap+discharge-unintercepted)	! total water fluxes
!check and delwater should be the same magnitude

write(94,'(I6,13(",",F10.5))')nint(daytime(1)),modess,runoff*1e3,sum(ppt),totevap,&
delwater,discharge,currentwater,check,check-delwater,modwet,unintercepted,canopy_store !all output in mm d-1 (canopys_store=mm)

prevwater=currentwater

CALL CIOUT(daytime(12))

RETURN
END
!-------------------------------------------------------------------
REAL FUNCTION DAYINT(series,nightflux) !integrate to give daily values
USE CLIM
implicit none
real series(steps),timestep,total,prev,dummy(steps+1),add,nightflux
integer t,test,gap

timestep=24.*60.*60./real(steps)  !# of seconds per timestep
total=0.
prev=0.; gap=0; test=1
DO t=1,steps			! interpolate if 1 or 2 data points are missing
	dummy(t)=series(t)
ENDDO
DO t=1,steps
	IF(dummy(t).lt.-500.)THEN	
		IF(partop(t).lt.20.)THEN 
			dummy(t)=nightflux
		ELSE
			gap=gap+1
			CALL GAPFIX(dummy,gap,t,0.)
		ENDIF
	ELSE
		gap=0
	ENDIF
	IF(gap.eq.3)test=0
ENDDO
IF(test.eq.1)THEN
	add=sum(dummy)-dummy(1)-dummy(steps)
	dayint=timestep*0.5*(dummy(1)+dummy(steps)+2.*add) ! trapezium rule
ELSE
	dayint=0.
ENDIF
!IF(dayint.lt.0.)THEN
!	write(*,*)'negative integration'
!ENDIF

RETURN
END
!-------------------------------------------------------------------
SUBROUTINE GAPFIX(series,gap,point,sub)
USE CANOPY
implicit none
real series(steps),sub,next
integer point,gap
IF(point.eq.steps)THEN
	next=sub				!substitute for next reading if at end of series
ELSE
	next=series(point+1)	!next reading
ENDIF
IF(gap.eq.1)THEN
	series(point)=0.5*(series(point-1)+next)
ENDIF
IF(gap.eq.2)THEN
	series(point-1)=0.667*series(point-2)+0.333*next
	series(point)=0.333*series(point-2)+0.667*next
	IF(next.lt.-100)gap=3	!check if next point is negative and not picked up because it's at night
ENDIF
RETURN
END
!-------------------------------------------------------------------
SUBROUTINE CIOUT(day)
USE CANOPY
USE C13
real tavci(numl),day,ava,dummy(numl)
integer i

! time-averaged Ci by layer, weighted by net assimilation rate
tavci=sumaci/suma

write(70,'(11(F9.2,","))')day,(tavci(i),i=1,numl)

RETURN
END

! Functions needed by g95

REAL FUNCTION SIND(x)
	SIND = SIN(x/180.0*3.141592)
	RETURN
END FUNCTION

REAL FUNCTION COSD(x)
	COSD = COS(x/180.0*3.141592)
	RETURN
END FUNCTION

REAL FUNCTION ATAND(x)
	ATAND = ATAN(x/180.0*3.141592)
	RETURN
END FUNCTION
