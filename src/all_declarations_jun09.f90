MODULE CLIM
USE CANOPY
real temptop(steps),tempbot(steps),partop(steps),wdtop(steps),wdbot(steps),dummy2(steps)
real rnet(steps),fC(steps),fW(steps),windsp(steps),coa(steps),st(steps),st2(steps),swrad(steps)
real gbw(numl,steps),daytime(steps),winddir(steps),mresp(steps),co2flux(steps),pressure(steps)
real par_ratio,Lin(steps),LEsoil(steps),wetev(steps),ppt(steps),mfdiff(steps),avtemp
integer daynum
data par_ratio/4.6/	!4.6 umol J-1
data Lin/steps*-999./
data pressure/steps*100000./  !atmospheric pressure, Pa
data mfdiff/steps*-999./
END MODULE
!
MODULE VEG
USE CANOPY
real gplant,canht,minlwp,towerht,totla,co2amb,totn,capac,lat,outt,altitude,avN
real gppt(steps),respt(steps),transt(steps),totass,totres,totevap,modRnet(steps),gsagg(steps)
real lai(numl),Nla(numl),layerht(numl),dimen,LWPstore(numl),ess(steps),soiletmm(steps)
real canopy_soil_resistance(numl),PWPstore(numl),lafrac(numl),nfrac(numl),sensh(steps)
real flux(steps,numl), C3KAP_VC,C3KAP_VJ, C4KAP_VC,C4KAP_VP,C4KAP_VJ, C3iota, C4iota
integer conductivity,nlink, c3(numl)
data nlink/1/
data conductivity/1/	! 1=conductivity set; 0=conductance set
data dimen/0.08/		! HF leaf dimension
data LWPstore/numl*0./	!initial LWP=0.
ENDMODULE

MODULE METEO
real temp,wdef,rad,gbb,par,psil,psis,nit,la,atmos_pres,head,gi,Rcon,psip,cp,hflux,cbsx
data Rcon/8.3144/	!gas constant
data head/0.009807/	! head of pressure  (MPa/m)
data gi/1.0/		!mesophyll conductance (m s-1) set high, and thus ignored, /0.0025/ old value
data cp/0.001012/	! spec. heat of air, (KJ g-1 K-1)
END MODULE

MODULE METAB
real ht,rn,vcm,vpm,vjm,rsoil,rplant,ci,et,an,resp,opttemp
real propvcm,propvjm,propresp,layer_capac,Vckurtosis,Vjkurtosis
integer c3path
data propvcm/0.5/
data propvjm/0.5/
data propresp/1.0/
data opttemp/30.0/	!metabolic temperature optimum
data Vckurtosis/0.143/ !kurtosis of Vcmax temp response
data Vjkurtosis/0.172/ !kurtosis of Jmax temp response
END MODULE

MODULE CUP
real vcmax,vjmax,vpmax,kc,ko,kp,gamma,gs2,lt,cihold,cbs,gammax
END MODULE

MODULE error
integer errday,errlayer
real temperror
real storeagr(1200,48,10)
real storeet(1200,48,10)
real errtime,lowg,lcp
ENDMODULE

MODULE C13
USE CANOPY
real sumaci(numl),suma(numl),sumet(numl),numsum(numl)
ENDMODULE

MODULE ANNUAL
real annualgpp,annualle,daygpp(14),annualrm,annualrg,annualloss
integer thawdays(7)
ENDMODULE

MODULE LWPINTEGRATOR
INTEGER KMAXX,MAXSTP,NMAX
REAL TINY
PARAMETER (MAXSTP=10000,NMAX=2,KMAXX=200,TINY=1.e-30)
END MODULE LWPINTEGRATOR

MODULE IRRADIANCE_SUNSHADE
USE canopy
real longem(steps,numl),parabs_sun(steps,numl),nirabs(steps,numl)
real parabs_shade(steps,numl),leaf_sun(steps,numl),soilnet(steps)
real parrefl,partrans,nirrefl,nirtrans,soilpar,soilnir,spherical,checkpar(steps,numl)
data parrefl/0.11/	!Baldocchi
data partrans/0.16/
!data parrefl/0.09/	!Jones p.31
!data partrans/0.06/
data nirrefl/0.43/
data nirtrans/0.26/
data soilpar/0.033/
data soilnir/0.023/
data spherical/2./	! spherical leaf angle distribution has a value 2.
END MODULE

MODULE HOURSCALE
integer hour
real Qh,Qe,Qn,Qc
real hourtemp,hourwind,hourvpd,hourrad,hourpress,hourtime,hourts,evaprsoil,hourppt
real freeze,dayevap,runoff,totet,discharge,surface_watermm,canopy_store,evap_store
real unintercepted,hourrnet,gaw,gws,overflow,underflow
data freeze/273.15/
END MODULE HOURSCALE

MODULE HYDROL
USE CANOPY
real sandpc(core),claypc(core)
END MODULE HYDROL

MODULE SOIL_STRUCTURE
USE CANOPY
integer rootl,saxton,thawed,extra
real through_fall,max_storage,drythick,rootmass(core),rootdens,rootbiomass,surfbiomass
real thickness(core),soiltemp(core),thermal,draincheck(core),soiltemp_nplus1(core)
real organicfrac(core),mineralfrac(core),waterfrac(core),iceprop(core),rootresist
real SWP(core),conduc(core),soilR(core),soilR1(core),soilR2(core),field_capacity(core)  
real waterloss(core),watergain(core),snow,rootrad,rootlength(core),watericemm(layer)
real weighted_SWP,weighted_soilR,fraction_uptake(core),porosity(core),rootfrac(core)
real pptgain(core),abovebelow	!ratio of above ground canopied area to below ground rooted area
real wettingtop(10), wettingbot(10),soil_resistance
data wettingtop/10*0./	!surface layer wetting fronts
data wettingbot/10*0./	!surface layer wetting fronts
data thermal/1.58/	! thermal conductivity, W m-1 K-1, converted to J m-1 K-1 h-1
data draincheck/core*0.70/	! 0.7 fraction of porosity above which gravitational drainage occurs
data through_fall/0.70/	!fraction of ppt which is not intercepted by canopy
data max_storage/1.0/	!mm of water storage in canopy
data rootresist/400./	! 
data saxton/0/	! saxton water retention equation are off by default
data abovebelow/1./	
data rootdens/0.5e6/	! root density, g biomass m-3 root
END MODULE SOIL_STRUCTURE

MODULE INTEGRATOR
!USE SOIL_STRUCTURE
INTEGER KMAXX,MAXSTP,NMAX
REAL TINY
PARAMETER (MAXSTP=10000,NMAX=4,KMAXX=200,TINY=1.e-30)
END MODULE INTEGRATOR

MODULE ENERGY_CONSTANTS
real cpair,vonkarman,albedo,boltz,emiss,Vw,grav
data albedo/0.2/
data cpair/1012./		!J kg-1 K-1
data vonkarman/0.41/	!von Karman's constant
data boltz/5.6703e-8/	! SB constant W m-2 K-4
data emiss/0.96/		!emissivity
data Vw/18.05e-6/	!partial molal volume of water, m3 mol-1 at 20C
data grav/9.8067/	!acceleration due to gravity, m s-1
END MODULE

MODULE SOILINFO
integer slayer
real soilpor,liquid,unsat,drainlayer
END MODULE SOILINFO

MODULE DREaMdecs
integer cells,stemclass,sub,month
parameter (cells=1)	!number of grid cells
parameter (sub=12)	!number of time steps per year
parameter (stemclass=10)	!number of stem size classes
real foliageC,stemC,rootC,labileC	!C content, g/m2
real Ps,Fabs,Rg,Rm,Fallo,Sallo,Rallo,Floss,Sloss,Rloss	!C fluxes, g/m2/t
real folCN,stemCN,rootCN	!C:N ratios
real folTO,stemTO,rootTO	!annual turnover
real stemdist(cells,stemclass)	! distribution of stems in each size class, no/ha
real LMA	!leaf mass per area g m-2
real Afoliage,Aroots,Astem	!allocation to foliage and roots and stems
real maxlai,abscise		!maximum lai and abscission
integer responsetime	!allocation only altered after this many days
END MODULE

MODULE SNOWINFO
real f_I(3),snowheat,snowweight,snowheight,W_L(3),totsoilflux,Tsnow(3)
real delta_t
real snowh(3),snoww(3),snowht(3),ro_snow(3),soilflux,totwaterflux,waterflux(3)
END MODULE SNOWINFO
