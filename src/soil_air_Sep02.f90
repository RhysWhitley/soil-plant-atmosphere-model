SUBROUTINE BOUNDARY()
! Determine boundary layer conductances at each canopy layer
USE VEG
USE CLIM
USE METEO
IMPLICIT NONE

INTEGER i, t
REAL roughl
REAL alpha, thick, xx, tempx
REAL Dwv, store, mult
REAL u(numl,steps)
      
alpha = 4.
roughl = 0.075*towerht		! Tower height is canht+4

DO t=1,steps
	DO i=1,numl
		gbw(i,t)=0.
	ENDDO
    xx = 0.
    store = 1.0
    DO i=1,numl
		xx = xx+1.
		tempx = temptop(t)-0.1111*(xx-1)*(temptop(t)-tempbot(t))
		Dwv = .0000242*(((tempx+273.15)/293.15)**1.75)*101300./atmos_pres	! Jones p 51
        ! Windspeed in layer - no decay in windspeed in this open stand
        !u(i,t) = windsp(t)
		mult = EXP(alpha*(layerht(i)/towerht-1.))
		u(i,t) = windsp(t)*mult
		IF(u(i,t).le.0.) THEN
			WRITE(*,*)daynum,t,windsp(t) 
			WRITE(*,'("Windspeed ERROR")')
		ENDIF
		thick = 0.004*(dimen/u(i,t))**0.5	! Boundary layer thickness
        ! Conductance to water vapour (m s-1 - not mol m-2 s-1 as in Amthor p.333)
        ! i.e. remove P/RT
		gbw(i,t) = Dwv/thick
    ENDDO
ENDDO
RETURN

END
!-------------------------------------------------------------------------------------!
SUBROUTINE SOILFC()		! From Saxton et al (1986)
! Waterfrac is m3 m-3, soilwp is MPa
! Conductivity corrected 12 Feb 1999
USE HYDROL
USE SOIL_STRUCTURE
USE HOURSCALE	! Contains soil evap estimate Qe
USE CLIM
USE METEO
IMPLICIT NONE

INTEGER i
REAL soil_WP, soil_sat, maxloss, unsat, rs, Lsoil, rs2, pi
REAL potA, potB, cond1, cond2, cond3, numsecs, lambda, watercontent
REAL A, B, CC, D, E, F, G, H, J, K, P, Q, R, T, U, V, mult1, mult2, mult3

mult1=100.;  mult2=2.778e-6; mult3=1000.0;  
A=-4.396;  B=-0.0715; CC=-4.880e-4; D=-4.285e-5; E=-3.140; F=-2.22e-3; G=-3.484e-5; H=0.332 
J=-7.251e-4; K=0.1276; P=12.012; Q=-7.551e-2; R=-3.895; T=3.671e-2; U=-0.1103; V=8.7546e-4

! --- RESET ---
waterloss = 0.
watergain = 0.
pptgain = 0.

DO i=1,core	
	potA = EXP(A+B*claypc(i)+CC*sandpc(i)*sandpc(i)+D*sandpc(i)*sandpc(i)*claypc(i))*100.
	potB = E + F * claypc(i) *claypc(i) + G * sandpc(i) * sandpc(i) * claypc(i)	! Correct to saxton eqn 6

	cond1 = mult2
	cond2 = P+Q*sandpc(i)
	cond3 = R+T*sandpc(i)+U*claypc(i)+V*claypc(i)*claypc(i)
	IF(waterfrac(i).lt.0.05) THEN	! Avoid underflow problem
		conduc(i) = 1e-30
	ELSE
		conduc(i) = cond1*(EXP(cond2+cond3/waterfrac(i))) ! 2. Soil conductivity (m s-1 ) */
	ENDIF
	IF(extra.eq.0) THEN	! Porosity has not been specified and is now estimated via Saxton equations
        porosity(i) = H + J*sandpc(i) + K * LOG10(claypc(i)) 
	ENDIF  ! Otherwise, porosity was specified in the soil driver file
ENDDO

RETURN

END

! --------------------------------------------------------------------
SUBROUTINE SWPSOILR()
! Waterfrac is m3 m-3, soilwp is MPa
! Conductivity corrected 12 Feb 1999
USE HYDROL
USE SOIL_STRUCTURE
USE HOURSCALE	!contains soil evap estimate Qe
USE CLIM
USE METEO
IMPLICIT NONE

INTEGER i
REAL soil_WP, soil_sat, maxloss, unsat, rs, Lsoil, rs2, pi, maxdrain
REAL potA, potB, cond1, cond2, cond3, numsecs, lambda, watercontent, rrcheck
REAL A, B, CC, D, E, F, G, H, J, K, P, Q, R, T, U, V, mult1, mult2, mult3

mult1=100.; mult2=2.778e-6; mult3=1000.0;  A=-4.396;  B=-0.0715
CC=-4.880e-4; D=-4.285e-5; E=-3.140; F=-2.22e-3;  G=-3.484e-5
H=0.332; J=-7.251e-4; K=0.1276; P=12.012; Q=-7.551e-2; R=-3.895
T=3.671e-2; U=-0.1103; V=8.7546e-4

numsecs = 3600.*24./REAL(steps)	!# 0f seconds per timestep
pi = 3.14159
lambda = 1000.*(2501.0-2.364*(hourtemp-273.15))	! latent heat of vapourisation, J kg-1
soilr = 0.

DO i=1,layer		! Find SWP without updating waterfrac yet (do that in waterthermal
	watercontent=(waterfrac(i)*thickness(i)-waterloss(i)+watergain(i))/thickness(i)
	potA = EXP(A+B*claypc(i)+CC*sandpc(i)*sandpc(i)+D*sandpc(i)*sandpc(i)*claypc(i))*100.
	!potB = E + F * claypc *claypc + G * sandpc * sandpc + G * sandpc * sandpc * claypc ! Incorrect Saxton TABLE 2
	potB = E + F * claypc(i) *claypc(i) + G * sandpc(i) * sandpc(i) * claypc(i)	! Correct to saxton eqn 6
	IF(watercontent.gt.0.) THEN
		soil_WP = -0.001*potA*watercontent**potB   !  Soil water potential (MPa)
	ELSE
		soil_WP = -9999.
	ENDIF
	IF(saxton.eq.0) THEN	! Turn off Saxton water potential
		soil_WP = -1.21+4.47*watercontent	! 7 June 1999, empirical relationship from Bev for Oregon soils
	ENDIF
	SWP(i) = soil_WP
	! Calculate soil-root hydraulic resistance
	Lsoil=conduc(i)/head	! Converts from ms-1 to m2 s-1 MPa-1
	IF(Lsoil.lt.1e-35) THEN	! Prevent floating point error
		soilr(i) = 1e35
	ELSE 
		! Exploited soil volume = 1.0 * soil depth; each canopy layer has lrv/numl root length and is soildp/numl deep - so the numl cancel
		!rs = sqrt(thickness(i)/(rootlength*pi))
		rs = SQRT(1./(rootlength(i)*thickness(i)*pi))
		!lrv = total length of roots, must be divided by number of layers in this case
		rs2 = LOG(rs/rootrad)/(2.0*pi*rootlength(i)*thickness(i)*Lsoil)
		soilR1(i) = rs2*1E-6*18*0.001	! convert from MPa s m2 m-3 to MPa s m2 mmol-1
		! Second component of below ground resistance related to root hydraulics
		!rrcheck = rootresist/(rootmass(i)*thickness(i)/abovebelow)
		SoilR2(i) = rootresist/(rootmass(i)*thickness(i)/abovebelow)
		soilR(i) = soilR1(i)+soilR2(i)
	ENDIF
ENDDO


RETURN
END
!-------------------------------------------------------------------------------------!
REAL FUNCTION SOILCOND(wf)		! For soil drainage integrator
! Waterfrac is m3 m-3, soilwp is MPa
! Conductivity corrected 12 Feb 1999
USE HYDROL
USE SOIL_STRUCTURE
USE HOURSCALE	! Contains soil evap estimate Qe
USE CLIM
USE METEO
USE SOILINFO
IMPLICIT NONE

INTEGER i
REAL soil_WP, soil_sat, maxloss, rs, Lsoil, rs2, pi, wf
REAL potA, potB, cond1, cond2, cond3, numsecs, lambda, watercontent
REAL A, B, CC, D, E, F, G, H, J, K, P, Q, R, T, U, V, mult1, mult2, mult3

mult1=100.;  mult2=2.778e-6; mult3=1000.0;  A=-4.396;  B=-0.0715
CC=-4.880e-4; D=-4.285e-5; E=-3.140; F=-2.22e-3;  G=-3.484e-5
H=0.332; J=-7.251e-4; K=0.1276; P=12.012; Q=-7.551e-2; R=-3.895
T=3.671e-2; U=-0.1103; V=8.7546e-4

potA = EXP(A+B*claypc(slayer)+CC*sandpc(slayer)*sandpc(slayer)+D*sandpc(slayer)*sandpc(slayer)*claypc(slayer))*100.
!potB = E + F * claypc *claypc + G * sandpc * sandpc + G * sandpc * sandpc * claypc ! Incorrect Saxton TABLE 2
potB = E + F * claypc(slayer) *claypc(slayer) + G * sandpc(slayer) * sandpc(slayer) * claypc(slayer)	! Correct to saxton eqn 6
cond1 = mult2
cond2 = P+Q*sandpc(slayer)
cond3 = R+T*sandpc(slayer)+U*claypc(slayer)+V*claypc(slayer)*claypc(slayer)
IF(wf.lt.0.05)THEN	! Avoid underflow problem
	soilcond = 1e-30
ELSE
	soilcond = cond1*(EXP(cond2+cond3/wf)) ! 2. Soil conductivity (m s-1 ) */
ENDIF

RETURN
END

